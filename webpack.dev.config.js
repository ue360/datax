process.env.NODE_ENV = 'development';
const FriendlyErrorsPlugin = require('friendly-errors-webpack-plugin');

const path = require('path');
const webpack = require('webpack');
const distPath = path.resolve(__dirname, './build');

const merge = require('webpack-merge') // 专用合并 webpack 配置的包
const webpackConfig = require('./webpack.config')
const mocker = require('webpack-api-mocker');
module.exports = merge(webpackConfig, {
	// 开发模式配置
	devServer: {
		before(app) {
			mocker(app, path.resolve('./mock/index.js'))
		},
		clientLogLevel: 'warning',
		inline: true,
		// 启动热更新
		hot: true,
		// 在页面上全屏输出报错信息
		overlay: {
			warnings: true,
			errors: true
		},
		// 显示 webpack 构建进度
		// progress: true,
		// dev-server 服务路径
		contentBase: distPath,
		compress: true,
		host: 'localhost',
		port: '8803',
		// 自动打开浏览器
		// open: true,
		// 可以进行接口代理配置
		proxy: {
			// "/api": {
			// 	"target": "http://localhost:8080/portal/web-debug/api/",
			// 	"changeOrigin": true,
			// 	"pathRewrite": {"^/api": ""}
			// }
			"/": {
				"target": "http://localhost:8080/portal/web-debug/",
				"changeOrigin": true,
				// "pathRewrite": {"^/api": ""}
				"pathRewrite": {"^/": "api/"}
			}
		},
		// 跟 friendly-errors-webpack-plugin 插件配合
		quiet: true,
		publicPath: '/'
	},
	plugins: [
		new FriendlyErrorsPlugin(),
		new webpack.HotModuleReplacementPlugin()
	]
})