module.exports = {
    [`POST /api/logs`]: (req, res) => {
        return res.json({
        	code: 200,
        	data: [{
	            id: 1,
	            name: '测试数据',
	            type: 1
	        }]
        });
    },

    [`POST /api/user`]: (req, res) => {
        return res.json({
        	code: 200,
        	data: {
	            id: 1,
	            name: 'kelvin'
	        }
        });
    },

    [`POST /api/login`]: (req, res) => {
        return res.json({
        	code: 200
        });
    },

    [`POST /api/logout`]: (req, res) => {
        return res.json({
        	code: 200
        });
    }
}