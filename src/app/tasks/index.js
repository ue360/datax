import {alias} from 'config';
import controller from './Task';
import SourceService from './source'

import template from './views/list.tpl.html';

alias({
    'tasks': 'ui-grid.json',
    'task_transform_rules': 'grid.json',
    'task_source': 'table.json',
    'task_local': 'table.json',
    'task_compare': 'table.json'
})
const router = ($stateProvider) => {
    $stateProvider.state('tasks', {
        url: '/tasks',
        views: {
            content: {
                template,
                controller,
                controllerAs: 'vm'
            }
        },
        parent: 'app',
        data: {
            name: 'tasks',
            title: '前置机'
        }
    });
}
router.$inject = ['$stateProvider'];

export default angular.module('app.tasks', [])
    .config(router)
    .factory('SourceService', SourceService)
    .name;