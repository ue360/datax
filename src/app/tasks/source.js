const source = ($q, $timeout, utils, http, dialog, api, ConfigApiService) => {
	function step(settings, config) {
        var deferred = $q.defer();
        var start = config == null || typeof config.data != 'object';

        config = config || {};
        settings = settings || {};
        settings = $.extend(true, {}, settings, config);
        settings.scope = settings.scope || {};

        var states = settings.scope.states;
        if (!states || states.length === 0) {
        	console.log('配置错误');
        	return;
        }
        var index = settings.scope.index;
        var state = states[index];
        var title = settings.title || state.title;
        var template = state.template;

        var data = $.extend(true, {}, settings.data, config.data || {});
        var options = {
            title: title,
            data: data,
            template: template,
            config: {
                width: 920
            }
        };

        if (settings.next) {
            var next = settings.next;
            var fn = settings.beforeSubmit;
            settings.beforeSubmit = function(data) {
                if (fn && fn.apply(this, arguments) === false) {
                    return false;
                }
                this.close();
                var _data = angular.extend({}, data);
                next({
                	data: _data
                });

                return false;
            };
            settings.next = null;
            delete settings.next;
        }
        angular.extend(options, settings);

        $timeout(function () {
            api.form(options).then(function (data) {
                deferred.resolve(data);
            });
        }, start ? 0 : 500);

        return deferred.promise;
    }
    function selectDataSource(config, options) {
    	var _loadData = function(params) {
				params = params || {};
				return api.gridLoad({
					name: 'database',
					params: params,
					scope: this,
					success: function(data, options) {
						var _scope = this;
						_scope.fields = angular.merge({}, _scope.fields, data.fields);
					}
				});
			};
    	var settings = {
				data: {
					type: 1
				},
				name: 'database_tables',
				// next: next,
				beforeSubmit: function(data) {
					var id = api.getGridSelectedId(this);
					if (!id) {
						dialog.alert('请选择数据源！');
						return false;
					}
					data.source = id;
				},
				scope: {
					index: options.index || 0,
					states: options.states,
					q: {},
					mapping: function(grid, row, col, field) {
						return api.mapping({
							grid: grid,
							row: row,
							col: col,
							field: field,
							scope: this
						});
					},
					add: function() {
						api.form({
							title: '添加数据源', 
							template: require('../database/views/form.tpl.html'),
							name: 'database_add',
							scope:{
								connTypes: utils.toArray(this.fields.connType)
							}
						}).then(function() {
							query();
						});
					},
					test: function(data) {

					},
					copyDataSource: function(params) {
						var _scope = this;
						_scope.formDisabled = true;
						http.post({
							name: 'task_copy',
							params: params,
							success: function() {
								_scope.close();
							}
						}).finally(function() {
							_scope.formDisabled = false;
						});
					},
					gridOptions: {
						paginationCurrentPage: api.page.pageNumber,
						paginationPageSize: api.page.pageSize,
						useExternalPagination: true,
						multiSelect: false,
						columns: [{
							name: '序号',
							width: 50,
							cellTemplate: '<div class="x-grid-inner">{{(grid.options.paginationCurrentPage - 1) * grid.options.paginationPageSize + (index + 1)}}</div>'
						}, {
							name: '数据源名称',
							width: 100,
							field: 'name'
						}, {
							name: '数据库名称',
							width: 100,
							field: 'dbName'
						}, {
							name: '数据库驱动',
							width: 100,
							field: 'connType',
							cellTemplate: '<div class="x-grid-inner" ng-bind-html="grid.appScope.mapping(grid, row, col)"></div>'
						}, {
							name: '数据库连接串',
							width: 200,
							field: 'jdbcUrl'
						}, {
							name: '用户名',
							width: 100,
							field: 'userName'
						}, {
							name: '状态',
							width: 100,
							field: 'isEnable',
							cellTemplate: '<div class="x-grid-inner" ng-bind-html="grid.appScope.mapping(grid, row, col)"></div>'
						}, {
							name: '操作',
							width: 100,
							field: 'jdbcUrl',
							cellTemplate: '<div class="x-grid-inner"><span class="btn-text"  ng-click="$event.stopPropagation();grid.appScope.test(grid.getData(row))">测试连接</span></div>'
						}],
						forceFit: false,
						onRegisterApi: function(api, grid) {
							var _scope = grid.appScope;
							_scope.api = api;

							api.pagination && api.pagination.on.paginationChanged(_scope, function(newPage, pageSize) {
								_scope.query(false);
							});
						}
					},
					query: function(resetPage) {
						if (resetPage === false || this.gridOptions.paginationCurrentPage == 1) {
							_loadData.call(this.q);
						} else {
							api.gridReset(null, this);
						}
					}
				},
				afterSubmit: function (tables, data) {
					data.tables = tables;
	                return data;
	            },
				resolveApply: false,
				resolveWait: function() {
					return _loadData.call(this);
				},
				config: {
					size: 960
				}
			};
		return step(settings, config).then(function(data) {
			options.next({
				data: data
			})
		});
    }
    function selectTables(config, options) {
		// var _loadData = function(params) {
		// 	params = params || {};
		// 	return api.gridLoad({
		// 		name: 'database_tables',
		// 		params: params,
		// 		scope: this,
		// 		success: function(data, options) {
		// 			var _scope = this;
		// 			_scope.fields = angular.merge({}, _scope.fields, data.fields);
		// 		}
		// 	});
		// };
		var settings = {
				data: {
				},
				name: 'database_fields',
				// next: stepFields,
				scope: {
					index: options.index || 1,
					states: options.states,
					prev: function() {
						this.close();
						options.prev(config, options.next);
					},
					gridOptions: {
						showHeader: false,
						enableRowSelection: true,
						enableRowHeaderSelection: false,
						multiSelect: false,
						dataView: {
							field: 'name',
							rowTemplate: '<div class="ui-grid-item-body"><div class="ui-item-ico"><i class="fa fa-table"></i></div><div class="ui-item-text">{{grid.getRowValue(row, \'name\')}}</div></div>',
							tools: []
						},
						data: config.data.tables,
						onRegisterApi: function(api, grid) {
							var _scope = grid.appScope.$parent;
							_scope.api = api;
						}
					}
				},
				beforeSubmit: function(data) {
					var id;
					if (!data.useSql && !(id = api.getGridSelectedId(this))) {
						dialog.alert('请选择表！');
						return false;
					}
					data.table = id;
				},
				// resolveApply: false,
				// resolveWait: function() {
				// 	return _loadData.call(this, this.data);
				// },
				afterSubmit: function (r, data) {
					data.fields = r.fields;
					data.tableName = r.name;
	                return data;
	            }
			};
		return step(settings, config).then(function(data) {
			options.next({
				data: data
			})
		});
	}
	function selectTable(config, options) {
		var settings = {
				data: {
				},
				name: 'task_compare',
				scope: {
					index: options.index || 1,
					states: options.states,
					prev: function() {
						this.close();
						options.prev(config);
					},
					gridOptions: {
						showHeader: false,
						enableRowSelection: true,
						enableRowHeaderSelection: false,
						multiSelect: false,
						dataView: {
							field: 'name',
							rowTemplate: '<div class="ui-grid-item-body"><div class="ui-item-ico"><i class="fa fa-table"></i></div><div class="ui-item-text">{{grid.getRowValue(row, \'name\')}}</div></div>',
							tools: []
						},
						data: config.data.tables,
						onRegisterApi: function(api, grid) {
							var _scope = grid.appScope.$parent;
							_scope.api = api;
						}
					}
				},
				beforeSubmit: function(data) {
					var id = api.getGridSelectedId(this);
					if (!id) {
						dialog.alert('请选择表！');
						return false;
					}
					data.table = id;
				}
			};
		return step(settings, config);
	}
	function selectFields(config, options) {
		// var _loadData = function(params) {
		// 		params = params || {};
		// 		return api.gridLoad({
		// 			name: 'database_fields',
		// 			params: params,
		// 			scope: this,
		// 			success: function(data, options) {
		// 				var _scope = this;
		// 				_scope.fields = angular.merge({}, _scope.fields, data.fields);
		// 				// $timeout(function() {
		// 				// 	_scope.selected();
		// 				// }, 0);
		// 			}
		// 		});
		// 	};
		var settings = {
				data: {
					
				},
				name: 'task_source',
				scope: {
					index: options.index || 2,
					states: options.states,
					prev: function() {
						this.close();
						options.prev(config);
					},
					gridOptions: {
						paginationCurrentPage: api.page.pageNumber,
						paginationPageSize: api.page.pageSize,
						useExternalPagination: true,
						enableFullRowSelection: false,
						columns: [{
							name: '字段名',
							width: 300,
							field: 'name'
						}, {
							name: '字段类型',
							width: 200,
							field: 'type'
						}],
						forceFit: false,
						data: config.data.fields,
						onRegisterApi: function(api, grid) {
							var _scope = grid.appScope;
							_scope.api = api;

							api.core.on.rowsRendered(_scope, function() {
								api.selection.selectAllRows();
							})
						}
					}
				},
				beforeSubmit: function(data, deferred) {
					var fields = api.getGridSelectedRecords(this);
					if (fields.length === 0) {
						dialog.alert('请选择字段');
						return false;
					}
					data.fields = fields;
				}
				// resolveApply: false,
				// resolveWait: function() {
				// 	return _loadData.call(this, this.data);
				// }
			};
		return step(settings, config);
	}
	function selectLocalFields(config, options) {
		var _loadData = function(params) {
				params = params || {};
				return api.gridLoad({
					name: 'database_local_fields',
					params: params,
					scope: this,
					success: function(data, options) {
						var _scope = this;
						_scope.fields = angular.merge({}, _scope.fields, data.fields);
						// $timeout(function() {
						// 	_scope.selected();
						// }, 0);
					}
				});
			};
		var settings = {
				data: {
					
				},
				name: 'task_local',
				// beforeSettings: function() {
				// 	var _scope = this;
				// 	var data = _scope.data;
				// 	_scope.$watch('data.table', function(newValue, oldValue) {
				// 		if (newValue != oldValue) {
				// 		}
				// 	});
				// },
				beforeSettings: function() {
					var _scope = this;
					var data = _scope.data;
					if (_scope.tables.length) {
						data.table = _scope.tables[0];
						_loadData.call(_scope, {
							table: _scope.tables[0].name
						});
					}
				},
				scope: {
					index: options.index || 1,
					states: options.states,
					tables: config.data.tables,
					prev: function() {
						this.close();
						options.prev(config);
					},
					selectTable: function($item, $model, $label, $event) {
						var _scope = this.$parent;
						var data = _scope.data;
						_loadData.call(_scope, {
							table: data.name
						});
					},
					selectAll: function(e) {
						var _scope = this;
						var data = _scope.data;

						data.table = '';
						if (e) {
							e.stopPropagation();
							var el = e.target;
							// var ipt = el.previousElementSibling;
							var ipt = angular.element(el).closest('div').find(':text');
							ipt.blur();
							setTimeout(function() {
								ipt.trigger('focus');
							}, 0)
						}
					},
					copyTable: function() {
						var _scope = this;
						var data = _scope.data;
						var vm = _scope.vm;
						data.copyed = !data.copyed;
						if (data.copyed) {
							if (vm.from) {
								data.copyedTable = vm.from.fields ? vm.from.table : '';
								_scope.gridOptions.data = vm.from.fields? [].concat(vm.from.fields) : [];
							}
						} else {
							_loadData.call(_scope, {
								table: data.table.name
							});
						}
					},
					add: function() {
						var _scope = this;
						var data = {
							name: ''
						}
						_scope.gridOptions.data.push(data);
						var api = _scope.api;
						var grid = api.grid;
						$timeout(function() {
							var row = grid.rows[grid.rows.length - 1];
							row.editable = true;
							row.isSelected = true;
						}, 0);
					},
					remove: function(index) {
						var _scope = this;
						_scope.gridOptions.data.splice(index, 1);
					},
					gridOptions: {
						paginationCurrentPage: api.page.pageNumber,
						paginationPageSize: api.page.pageSize,
						useExternalPagination: true,
						enableFullRowSelection: false,
						columns: [{
							name: '字段名',
							width: 300,
							field: 'name',
							// align: 'left',
							cellTemplate: '<div class="x-grid-inner" ng-if="!row.editable">{{grid.getCellValue(row, col)}}</div><div class="x-form-inner" ng-if="row.editable"><input type="text" ng-model="row.entity.name" class="x-ipt" required /></div>'
						}, {
							name: '字段类型',
							width: 200,
							field: 'type',
							cellTemplate: '<div class="x-grid-inner" ng-if="!row.editable">{{grid.getCellValue(row, col)}}</div><div class="x-form-inner" ng-if="row.editable"><select ng-model="row.entity.type" ng-init="row.entity.type=row.entity.type||grid.appScope.dataTypes[0].name" required ng-options="m.name as m.name for m in grid.appScope.dataTypes"></select></div>'
						}, {
							name: '操作',
							width: 200,
							cellTemplate: '<div class="x-grid-inner" ng-if="row.editable"><span class="btn-text" ng-click="grid.appScope.remove(index)"><i class="fa fa-remove"></i></span></div>'
						}],
						forceFit: false,
						onRegisterApi: function(api, grid) {
							var _scope = grid.appScope;
							_scope.api = api;
						}
					}
				},
				// resolveApply: false,
				// resolveWait: function() {
				// 	return _loadData.call(this, this.data);
				// },
				beforeSubmit: function(data, deferred) {
					var fields = api.getGridSelectedRecords(this);
					if (fields.length === 0) {
						dialog.alert('请选择字段');
						return false;
					}
					data.fields = fields;
				},
				resolveWait: {
					dataTypes: function() {
						return http.post({
							name: 'database_types'
						})
					}
				},
				resolveApplyScope: true
			};
		return step(settings, config);
	}
	let service = {
		dataSource: function() {
			var deferred = $q.defer();
			var states = [{
		        id: 'datasource',
		        title: '选择数据源',
		        template: require('./views/form_datasource.tpl.html'),
		        handler: function(config) {
		        	selectDataSource(config, {
		        		states: states,
		        		next: states[1].handler
		        	});
		        }
		    }, {
		        id: 'tables',
		        title: '选择数据表',
		        template: require('./views/from_tables.tpl.html'),
		        handler: function(config) {
		        	selectTables(config, {
		        		states: states,
		        		prev: states[0].handler,
		        		next: states[2].handler
		        	});
		        }
		    }, {
		        id: 'fields',
		        title: '选择字段',
		        template: require('./views/from_fields.tpl.html'),
		        handler: function(config) {
		        	selectFields(config, {
		        		states: states,
		        		prev: states[1].handler
		        	}).then(function(data) {
		        		deferred.resolve(data);
		        	});
		        }
		    }];
		    var state = states[0];
		    var handler = state.handler;
			handler();
			return deferred.promise;
		},
		dataLocalSource: function(from) {
			var deferred = $q.defer();
			var states = [{
		        id: 'datasource',
		        title: '选择数据源',
		        template: require('./views/form_datasource.tpl.html'),
		        handler: function(config) {
		        	config = config || {};
		        	config.vm = {
		        		copySource: true,
		        		from: from
		        	};

		        	selectDataSource(config, {
		        		states: states,
		        		next: states[1].handler
		        	});
		        }
		    }, {
		        id: 'fields',
		        title: '选择字段',
		        template: require('./views/to_fields.tpl.html'),
		        handler: function(config) {
		        	config = config || {};
		        	config.vm = {
		        		from: from
		        	};
		        	selectLocalFields(config, {
		        		states: states,
		        		prev: states[0].handler
		        	}).then(function(data) {
		        		deferred.resolve(data);
		        	});
		        }
		    }];
		    var state = states[0];
		    var handler = state.handler;
			handler();
			return deferred.promise;
		},
		dataCompareSource: function() {
			var deferred = $q.defer();
			var states = [{
		        id: 'datasource',
		        title: '选择数据源',
		        template: require('./views/form_datasource.tpl.html'),
		        handler: function(config) {
		        	selectDataSource(config, {
		        		states: states,
		        		next: states[1].handler
		        	});
		        }
		    }, {
		        id: 'tables',
		        title: '选择数据表',
		        template: require('./views/from_table.tpl.html'),
		        handler: function(config) {
		        	selectTable(config, {
		        		states: states,
		        		prev: states[0].handler
		        	}).then(function(data) {
		        		deferred.resolve(data);
		        	});
		        }
		    }];
		    var state = states[0];
		    var handler = state.handler;
			handler();
			return deferred.promise;
		},
		dataRules: function(type, params, rules, owner) {
			var loadData = function(params) {
				params = params || {};
				angular.extend(params, {
					type: type
				});
				return api.gridLoad({
					name: 'meta_rules',
					params: params,
					scope: this
				});
			};

			return api.form({
				title: '选择转换规则',
				template: require('./views/form_rules.tpl.html'),
				beforeSubmit: function(data, deferred) {
					var _scope = this;
					var _data = _scope.gridBufferOptions.data || [];
					var _rules = _data.map(function(r) {
						return {
							id: r.id,
							name: r.name,
							type: r.type,
							options: r.options
						}
					});

					data.rules = _rules;
				},
				scope: {
					q: {},
					fields: {
						type: [{
							id: 1,
							name: '校验规则'
						}, {
							id: 2,
							name: '转换规则'
						}]
					},
					query: function(resetPage) {
						var scope = this;
						if (resetPage === false || scope.gridOptions.paginationCurrentPage == 1) {
							loadData.call(this, scope.q);
						} else {
							api.gridReset(null, scope);
						}
					},
					hasSelected: function(apiName) {
						return api.hasGridSelected(this, apiName);
					},
					hasSelectedRecords: function(apiName) {
						return api.hasGridSelectedRecords(this, apiName);
					},
					gridOptions: {
						paginationCurrentPage: api.page.pageNumber,
						paginationPageSize: api.page.pageSize,
						useExternalPagination: true,
						multiSelect: false,
						columns: [{
							name: '序号',
							width: 50,
							field: 'id'
						}, {
							name: '规则名称',
							align: 'left',
							width: 200,
							field: 'name'
						}, {
							name: '类型',
							width: 100,
							field: 'type',
							cellTemplate: '<div class="x-grid-inner" ng-bind-html="grid.appScope.mapping(grid, row, col)"></div>'
						}, {
							name: '描述',
							width: 200,
							field: 'desc'
						}],
						forceFit: false,
						onRegisterApi: function(api, grid) {
							var scope = grid.appScope;
							scope.api = api;

							api.pagination && api.pagination.on.paginationChanged(scope, function(newPage, pageSize) {
								scope.query(false);
							});
						}
					},
					create: function() {
						var _scope = this;
						api.form({
							title: '新建规则',
							template: require('../meta/views/form_rule.tpl.html'),
							name: 'meta_rule_add'
						}).then(function() {
							_scope.query();
						});
					},
					custom: function() {
						var _scope = this;
						api.form({
							title: '自定义规则',
							template: require('./views/form_rule_custom.tpl.html'),
							name: 'meta_rule_custom_add',
							beforeSettings: function() {
								var _scope = this;
								ConfigApiService.reset().addJson();
								$timeout(function() {
									_scope.timer = true;
								});
							},
							beforeSubmit: function(data) {
								data.params = ConfigApiService.getResultArray();
							},
							scope: {
								addJson: ConfigApiService.addJson,
								jsonArray: ConfigApiService.jsonArray,
								getKey: ConfigApiService.getKey,
								removeJson: ConfigApiService.removeJson,
								movePrev: ConfigApiService.movePrev,
								moveNext: ConfigApiService.moveNext,
								timer: false,
								codeOptions: {
									mode: 'text/x-java',
									lineNumbers: true,
									matchBrackets: true
								}
							}
						}).then(function() {
							_scope.query();
						});
					},
					add: function() {
						var _scope = this;

						var _data = _scope.gridBufferOptions.data || [];
						var ids = _data.map(function(_r) {
							return _r.id;
						});

						var _add = function(data) {
							var i;
							if ((i = ids.indexOf(data.id)) === -1) {
								_scope.gridBufferOptions.data = [].concat(_data, data);
							} else {
								dialog.confirm('当前规则已在配置列表中，是否覆盖已有配置？').result.then(function() {
									if (r) {
										_scope.gridBufferOptions.data.splice(i, 1, data);
									}
								});
							}
						}

						var data = api.getGridSelectedRecord(_scope);
						var hasParams = data.noParams !== true;
						if (hasParams) {
							var settings;
							switch (data.type) {
								case 3:
									settings = {
										title: '规则配置',
										template: require('./views/form_rule_custom_params.tpl.html'),
										data: data,
										vm: {
											fields: owner.fields
										}
									}
									break;
								case 2:
									settings = {
										title: '规则配置',
										template: require('../meta/views/form_rule_transform.tpl.html'),
										data: data,
										beforeSettings: function() {
											ConfigApiService.reset().addJson();
										},
										beforeSubmit: function(data) {
											data.options = ConfigApiService.getResultArray();
										},
										scope: {
											addJson: ConfigApiService.addJson,
											jsonArray: ConfigApiService.jsonArray,
											getKey: ConfigApiService.getKey,
											removeJson: ConfigApiService.removeJson,
											movePrev: ConfigApiService.movePrev,
											moveNext: ConfigApiService.moveNext
										}
									}
									break;
								case 1:
								default:
									settings = {
										title: '规则配置',
										template: require('../meta/views/form_rule_params.tpl.html'),
										data: data
									}
							}
							settings && api.form(settings).then(function(data) {
								_add(data);
							});
						} else {
							_add(data);
						}
					},
					edit: function(grid, row) {
						var _scope = this;
						var data = grid.getData(row);
						var id = data.id;
						var _data = _scope.gridBufferOptions.data || [];
						var ids = _data.map(function(_r) {
							return _r.id;
						});
						var i = ids.indexOf(id);

						var settings;
						switch (data.type) {
							case 3:
								data.params = [{
									name: 'a'
								}, {
									name: 'b'
								}];
								var _params = data.params || [];
								var _tempParams = [].concat(_params);
								if (_data.extend == 1 && _tempParams.length) {
									_tempParams.shift();
								}
								settings = {
									title: '规则配置',
									template: require('./views/form_rule_custom_params.tpl.html'),
									data: data,
									vm: {
										fields: owner.fields,
										params: _tempParams
									},
									scope: {
										eitherShift: function() {
											var _scope = this;
											var vm = _scope.vm;
											var _data = _scope.data;
											if (_params.length === 0) return;
											if (_data.extend == 1) {
												vm.params.shift();
											} else {
												vm.params.unshift(_params[0]);
											}
										}
									}
								}
								break;
							case 2:
								settings = {
									title: '规则配置',
									template: require('../meta/views/form_rule_transform.tpl.html'),
									data: data,
									beforeSettings: function() {
										ConfigApiService.reset().setArrayData(data.options);
									},
									beforeSubmit: function(data) {
										data.options = ConfigApiService.getResultArray();
									},
									scope: {
										addJson: ConfigApiService.addJson,
										jsonArray: ConfigApiService.jsonArray,
										getKey: ConfigApiService.getKey,
										removeJson: ConfigApiService.removeJson,
										movePrev: ConfigApiService.movePrev,
										moveNext: ConfigApiService.moveNext
									}
								}
								break;
							case 1:
							default:
								settings = {
									title: '规则配置',
									template: require('../meta/views/form_rule_params.tpl.html'),
									data: data
								}
						}
						settings && api.form(settings).then(function(data) {
							_scope.gridBufferOptions.data.splice(i, 1, data);
						});
					},
					remove: function() {
						var _scope = this;
						var data = _scope.gridBufferOptions.data || [];
						var rs = api.getGridSelectedRecords(_scope, 'bufferApi');
						rs.forEach(function(r) {
							var id = r.id,
								i;
							var ids = data.map(function(_r) {
								return _r.id;
							});
							if ((i = ids.indexOf(id)) !== -1) {
								data.splice(i, 1);
								_scope.bufferApi.selections.removeKey(id);
								_scope.bufferApi.grid.selection.selectedCount--;
							}
						});
					},
					isFirst: function() {
						var r = api.getGridSelectedRecord(this, 'bufferApi');
						var data = this.gridBufferOptions.data;
						return data.indexOf(r) === 0;
					},
					isLast: function() {
						var r = api.getGridSelectedRecord(this, 'bufferApi');
						var data = this.gridBufferOptions.data;
						return data.indexOf(r) === data.length - 1;
					},
					movePrev: function() {
						api.movePrev(this, 'bufferApi', 'gridBufferOptions');
					},
					moveNext: function() {
						api.moveNext(this, 'bufferApi', 'gridBufferOptions');
					},
					mapping: function(grid, row, col, field) {
						return api.mapping({
							grid: grid,
							row: row,
							col: col,
							field: field,
							scope: this
						});
					},
					gridBufferOptions: {
						enableFullRowSelection: false,
						columns: [{
							name: '序号',
							width: 50,
							field: 'id'
						}, {
							name: '规则名称',
							align: 'left',
							width: 200,
							field: 'name'
						}, {
							name: '类型',
							width: 100,
							field: 'type',
							cellTemplate: '<div class="x-grid-inner" ng-bind-html="grid.appScope.mapping(grid, row, col)"></div>'
						}, {
							name: '描述',
							width: 200,
							field: 'desc'
						}, {
							name: '操作',
							width: 200,
							cellTemplate: '<span class="btn-label" ng-if="grid.getData(row).noParams!=true" ng-click="grid.appScope.edit(grid, row)">修改</span>'
						}],
						forceFit: false,
						onRegisterApi: function(bufferApi, grid) {
							var scope = grid.appScope;
							scope.bufferApi = bufferApi;

							if (bufferApi.selection) {
								grid.registerDataChangeCallback(function() {
									var visibleRows = bufferApi.core.getVisibleRows();
									grid.selection.selectAll = visibleRows.length > 0 && bufferApi.selection.getSelectedCount() === visibleRows.length;
								}, ['row']);
							}
						}
					}
				},
				resolveApply: false,
				resolveWait: function() {
					var _scope = this;
					return {
						grid: function() {
							return loadData.call(_scope);
						},
						gridBuffer: function() {
							if (rules) {
								_scope.gridBufferOptions.data = [].concat(rules);
								return;
							}
							return api.gridLoad({
								name: 'task_transform_rules',
								params: params,
								options: 'gridBufferOptions',
								apiName: 'bufferApi',
								scope: _scope
							});
						}
					}
				},
				config: {
					width: 960
				}
			})
		}
	}
	return service;
}

source.$inject = ['$q', '$timeout', 'ui.utils', 'ui.http', 'ui.dialog', 'ui.api', 'ConfigApiService'];

export default source;