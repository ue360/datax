import {Inject} from 'plugins'
import config from 'config';

@Inject('$scope', '$state', 'ui.utils', 'ui.http', 'ui.dialog', 'ui.api')
export default class Database {
	constructor($scope, $state, utils, http, dialog, api) {
		var q = $scope.q = {};

		var loadData = $scope.loadData = function(params) {
				api.gridLoad({
					name: 'database', 
					params: params,
					scope: $scope,
	                success: function(data, options) {
	                    var _scope = this;
	                    _scope.fields = data.fields;
	                }
				});
			};
		

	    var query = $scope.query = function(resetPage) {
			if (resetPage === false || $scope.gridOptions.paginationCurrentPage == 1) {
				loadData(q);
			} else {
				api.gridReset(null, $scope);
			}
		}

		$scope.add = function() {
			api.form({
				title: '添加数据源', 
				template: require('./views/form.tpl.html'),
				name: 'database_add',
				scope:{
					connTypes: utils.toArray($scope.fields.connType)
				}
			}).then(function() {
				query();
			});
		}

		$scope.edit = function() {
			var id = api.getGridSelectedId($scope);
			api.form({
				title: '修改数据源', 
				template: require('./views/form.tpl.html'),
				name: 'database_edit',
				scope:{
					connTypes: utils.toArray($scope.fields.connType)
				},
				resolveWait: http.post({
					name: 'database_item',
					params: {
						id: id
					}
				})
			}).then(function() {
				query(false);
			});
		}
		$scope.remove = function() {
			var ids = api.getGridSelectedIds($scope);
			dialog.confirm('确定要移除选中的数据源？').result.then(function(r) {
				if (r) {
					http.post({
						name: 'database_remove',
						params: {
							ids: ids
						},
						success: function() {
							query(false);
						}
					})
				}
			});
		}

		$scope.start = function() {
			var ids = api.getGridSelectedIds($scope);
			dialog.confirm('确定要开启选中的数据源？').result.then(function(r) {
				if (r) {
					http.post({
						name: 'database_start',
						params: {
							ids: ids
						},
						success: function() {
							query(false);
						}
					})
				}
			});
		}
		$scope.stop = function() {
			var ids = api.getGridSelectedIds($scope);
			dialog.confirm('确定要停用选中的数据源？').result.then(function(r) {
				if (r) {
					http.post({
						name: 'database_stop',
						params: {
							ids: ids
						},
						success: function() {
							query(false);
						}
					})
				}
			});
		}

		$scope.gridOptions = {
			paginationCurrentPage: api.page.pageNumber,
			paginationPageSize: api.page.pageSize,
			useExternalPagination: true,
			columns: [{
				name: '序号',
				width: 50,
				cellTemplate: '<div class="x-grid-inner">{{(grid.options.paginationCurrentPage - 1) * grid.options.paginationPageSize + (index + 1)}}</div>'
			}, {
				name: '数据源名称',
				width: 100,
				field: 'name'
			}, {
				name: '数据库名称',
				width: 100,
				field: 'dbName'
			}, {
				name: '数据库驱动',
				width: 100,
				field: 'connType',
				cellTemplate: '<div class="x-grid-inner" ng-bind-html="grid.appScope.mapping(grid, row, col)"></div>'
			}, {
				name: '数据库连接串',
				width: 200,
				field: 'jdbcUrl'
			}, {
				name: '用户名',
				width: 100,
				field: 'userName'
			}, {
				name: '状态',
				width: 100,
				field: 'isEnable',
				cellTemplate: '<div class="x-grid-inner" ng-bind-html="grid.appScope.mapping(grid, row, col)"></div>'
			}, {
				name: '操作',
				width: 100,
				field: 'jdbcUrl',
				cellTemplate: '<div class="x-grid-inner"><span class="btn-text"  ng-click="grid.appScope.testConn(row)">测试连接</span></div>'
			}],
			onRegisterApi: function(api) {
				$scope.api = api;
				api.pagination && api.pagination.on.paginationChanged($scope, function(newPage, pageSize) {
					query(false);
				});
			}
		};

		$scope.testConn = function(row){
			http.post({
				name: 'database_testConn',
				params: row.entity
			}).then(function(data) {
				dialog.alert(data.data);
			});
		}

		$scope.mapping = function(grid, row, col, field) {
			return api.mapping({
				grid: grid,
				row: row,
				col: col,
	            field: field,
				scope: $scope
			});
		}

		$scope.hasSelected = function() {
			return api.hasGridSelected($scope);
		}
		$scope.hasSelectedRecords = function() {
			return api.hasGridSelectedRecords($scope);
		}

		query();
	}
}