import {alias} from 'config';
import controller from './Database'

import template from './views/list.tpl.html';

alias({
    'database': 'grid.json',
    'database_filter': 'field.json',
    'database_add': 'datasource/save',
    'database_edit': 'datasource/save',
    'database_item': 'datasource/get',
    'database_remove': 'datasource/delete',
    'database_start': 'datasource/start',
    'database_stop': 'datasource/stop',
    'database_check_name': 'datasource/checkName',
    'database_testConn': 'datasource/sourceTest',
    'database_tables': 'q-tables.json',
    'database_fields': 'model.json',
    'database_local_fields': 'grid.json',
    'database_types': 'fields.json'
})
const router = ($stateProvider) => {
    $stateProvider.state('database', {
        url: '/database',
        views: {
            content: {
                template,
                controller,
                controllerAs: 'vm'
            }
        },
        parent: 'app',
        data: {
            name: 'config',
            title: '数据源配置'
        }
    });
}
router.$inject = ['$stateProvider'];

export default angular.module('app.database', [])
    .config(router)
    .name;