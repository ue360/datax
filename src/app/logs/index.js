import {alias} from 'config';
import controller from './Logs'

import template from './views/list.tpl.html';

alias({
	'logs': 'grid.json'
})
const router = ($stateProvider) => {
	$stateProvider.state('logs', {
		url: '/logs',
		views: {
			content: {
				template,
				controller,
				controllerAs: 'vm'
			}
		},
		parent: 'app',
		data: {
			name: 'config',
            title: '系统日志'
		}
	});
}
router.$inject = ['$stateProvider'];

export default angular.module('app.logs', [])
	.config(router)
	.name;
