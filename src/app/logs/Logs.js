import {Inject} from 'plugins'

@Inject('$scope', '$state', 'ui.http', 'ui.dialog', 'ui.api')
export default class Logs {
	$onInit() {
		
	}
	constructor($scope, $state, http, dialog, api) {
		var q = $scope.q = {};

		var loadData = $scope.loadData = function(params) {
				api.gridLoad({
					name: 'logs', 
					params: params,
					scope: $scope,
	                success: function(data, options) {
	                    var _scope = this;
	                    _scope.fields = data.fields;
	                }
				});
			};

	    var _cacheParams;

	    // 执行简单查询时，需要清空高级查询表单
	    var query = $scope.query = function(resetPage) {
	    	if (resetPage) {
	            _cacheParams = null;
			}
			if (resetPage === false || $scope.gridOptions.paginationCurrentPage == 1) {
				var _params = angular.extend({}, q, _cacheParams);
	            loadData(_params);
			} else {
				api.gridReset(null, $scope);
			}
		}
	    
	    $scope.cantPageForward = function () {
	        return $scope.gridOptions.data.length < api.page.pageSize;
	    }

		$scope.gridOptions = {
			paginationCurrentPage: api.page.pageNumber,
			paginationPageSize: api.page.pageSize,
			useExternalPagination: true,
			paginationTemplate: '<div class="x-grid-footer" grid-pager><div class="x-grid-pages fl"><span class="x-grid-btn" ng-click="cantPageBackward()||pagePrevious()" ng-disabled="cantPageBackward()"><i class="fa fa-angle-left"></i> 上一页</span><span class="x-grid-btn" ng-click="grid.appScope.cantPageForward()||pageNext()" ng-disabled="grid.appScope.cantPageForward()">下一页 <i class="fa fa-angle-right"></i></span></div><div class="x-grid-pages"><span class="x-grid-btn" ng-click="cantPageBackward()||pagePrevious()" ng-disabled="cantPageBackward()"><i class="fa fa-angle-left"></i> 上一页</span><span class="x-grid-btn" ng-click="grid.appScope.cantPageForward()||pageNext()" ng-disabled="grid.appScope.cantPageForward()">下一页 <i class="fa fa-angle-right"></i></span></div></div>',
			columns: [{
				name: '序号',
				width: 50,
				cellTemplate: '<div class="x-grid-inner">{{(grid.options.paginationCurrentPage - 1) * grid.options.paginationPageSize + (index + 1)}}</div>'
			}, {
				name: '操作对象',
				align: 'left',
				width: 100,
				field: 'username'
			},{
				name: '模块名称',
				width: 150,
				field: 'module'
			},{
				name: '功能名称',
				width: 200,
				field: 'tfunction'			
			},{
				name: '开始操作时间',
				width: 150,
				field: 'startDate',
				cellTemplate: '<div class="x-grid-inner">{{grid.getCellValue(row, col)| date:"yyyy-MM-dd HH:mm:ss"}}</div>'
			},{
				name: '结束操作时间',
				width: 150,
				field: 'endDate',
				cellTemplate: '<div class="x-grid-inner">{{grid.getCellValue(row, col)| date:"yyyy-MM-dd HH:mm:ss"}}</div>'
			},{
				name: '操作结果',
				width: 100,
				field: 'state',
	            cellTemplate: '<div class="x-grid-inner" ng-bind-html="grid.appScope.mapping(grid, row, col, \'status\')"></div>'
			},{
				name: '操作',
				width: 100,
				field: 'id',
				cellTemplate: '<div class="x-grid-inner"><span class="btn-text" ng-click="grid.appScope.preview(grid.getCellValue(row, col))">详情</span></div>'
			}],
			forceFit: false,
			onRegisterApi: function(api) {
				$scope.api = api;
				api.pagination && api.pagination.on.paginationChanged($scope, function(newPage, pageSize) {
					query(false);
				});
			}
		};

	    $scope.mapping = function(grid, row, col, field) {
	        return api.mapping({
	            grid: grid,
	            row: row,
	            col: col,
	            field: field,
	            scope: $scope
	        });
	    }

	    // 缓存高级查询表单
	    $scope.adv = function() {
	        api.form({
	            title: '高级检索',
	            template: require('./views/form_query.tpl.html'),
	            config: {
	                width: 320,
	                backdrop: true
	            },
				data: _cacheParams,
	            beforeSubmit: function(data, deferred) {
	                deferred.resolve(data);
	                this.close();
	            }
	        }).then(function(data) {
	            _cacheParams = angular.extend({}, data);
	            query();
	        });
	    }

		$scope.preview = function(id) {
			api.form({
				title: '日志详情',
				template: require('./views/form_preview.tpl.html'),
				resolveWait: http.post({
					name: 'log_preview',
					params: {
						id: id
					}
				})
			})
		}

		$scope.hasSelected = function() {
			return api.hasGridSelected($scope);
		}
		$scope.hasSelectedRecords = function() {
			return api.hasGridSelectedRecords($scope);
		}

		query();
	}
}