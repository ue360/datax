import login from './login';
import logs from './logs';
import misc from './misc';
import notice from './notice';
import sms from './sms';
import database from './database';
import settings from './settings';
import org from './org';
import meta from './meta';
import dictionary from './dictionary';
import tasks from './tasks';
import api from './api';

export default [
	login,
	logs,
	misc,
	notice,
	sms,
	database,
	settings,
	org,
	meta,
	dictionary,
	tasks,
	api
]