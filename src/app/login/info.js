import template from './views/info.tpl.html'

const info = (User) => {
	return {
		restrict: 'AE',
		template,
		link: (scope, element) => {
			scope.user = User.get();
		}
	}
}
info.$inject = ['User'];

export default info;