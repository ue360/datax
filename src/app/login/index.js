import {alias} from 'config'
import controller from './Login'
import User from './user'
import info from './info'
import template from './views/form.tpl.html';

alias({
	'login': 'grid.json',
	'login_user': 'field.json',
	'logout': 'grid.json'
})

const router = ($stateProvider) => {
	$stateProvider.state('login', {
		url: '/login',
		views: {
			root: {
				template,
				controller,
				controllerAs: 'vm'
			}
		},
		data: {
			title: '登录',
            htmlId: 'extr-page'
		}
	});
}
router.$inject = ['$stateProvider'];

export default angular.module('app.login', [])
	.config(router)
	.factory('User', User)
	.directive('loginInfo', info)
	.name;
