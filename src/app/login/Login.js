import {Inject} from 'plugins'

@Inject('$scope', '$state', 'ui.dialog', 'ui.http')
export default class Login {
	constructor($scope, $state, dialog, http) {
		var user = $scope.user = {};
	    $scope.submitted = $scope.formDisabled = false;
		$scope.submit = function(valid) {
			$scope.submitted = true;
			if (valid) {
				$scope.formDisabled = true;
				dialog.mask();
				http.post({
					name: 'login',
					params: user
				}).success(function(data) {
					$state.go('logs');
		        }).error(function(){
		        	$scope.submitted = $scope.formDisabled = false;
		        }).finally(function() {
		        	dialog.unmask();
		        });
			}
		}
		
		$scope.interacted = function(field) {
			return $scope.submitted || field.$dirty || field.$touched;
		};
	}
}