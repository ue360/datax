import {alias} from 'config';
import Settings from './Settings';
import SettingsList from './SettingsList';

import templateLayout from './views/layout.tpl.html';
import template from './views/list.tpl.html';

alias({
    'settings': 'sys/configList',
    'settings_add': 'sys/create',
    'settings_edit': 'sys/updateConfig',
    'settings_item': 'sys/read',
    'settings_remove': 'sys/batchDeleteConfigs',
    'org_config_valid': 'sys/checkKey',
    'settings_type_add': 'basic/configuration/create',
    'settings_type_remove': 'basic/configuration/delete',
    'settings_type_edit': 'basic/configuration/create',
    'settings_type': 'basic/configuration/read',
    'settings_isExistName': 'basic/configuration/checkWarningName'
})
const router = ($stateProvider) => {
    $stateProvider
        .state('settings', {
            abstract: true,
            views: {
                content: {
                    template: templateLayout,
                    controller: Settings,
                    controllerAs: 'vm'
                }
            },
            parent: 'app',
            data: {
                name: 'config',
                title: '系统配置'
            }
        })
        .state('settings.list', {
            url: '/settings/list',
            views: {
                "primary": {
                    template,
                    controller: SettingsList,
                    controllerAs: 'vm'
                }
            },
            data: {
                name: 'config',
                title: '基础配置'
            }
        });
}
router.$inject = ['$stateProvider'];

export default angular.module('app.settings', [])
    .config(router)
    .name;