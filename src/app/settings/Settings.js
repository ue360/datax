import {Inject} from 'plugins'
import config from 'config';

@Inject('$scope', '$state', 'ui.http', 'ui.dialog', 'ui.api')
export default class Settings {
	constructor($scope, $state, http, dialog, api) {
		var dataSource = api.tree({
			scope: $scope,
			type: 'tree',
			url: 'tree.php'
		});
		dataSource.bind('requestEnd', function() {
			var tree = $scope.tree;
			setTimeout(function() {
				var root = dataSource.at(0);
				var el = tree.findByUid(root.uid);
				tree.select(el);
			}, 0);
		});
	//	$scope.$on("kendoWidgetCreated", function(event, widget) {
	//		var tree = $scope.tree;
	//		if (widget === tree) {
	//			var dataSource = tree.dataSource;
	//			setTimeout(function() {
	//				var root = dataSource.at(0);
	//				var el = tree.findByUid(root.uid);
	//				tree.select(el);
	//			}, 0);
	//		}
	//	});

		$scope.treeReload = function() {
			api.treeReload.apply($scope, arguments);
		}

		var getTreeNode = $scope.getTreeNode = function() {
			return $scope.selectedTreeNode;
		}

		$scope.isNodeRoot = function() {
			var node = getTreeNode();
			return !node || node.id == -1;
		}

		$scope.add = function() {
			var node = getTreeNode();
			var id = node.id;
			api.form({
	            title: '新建分类',
	            template: require('./views/form_type.tpl.html'),
	            name: 'settings_type_add',
	            data: {//jinset
	                parentId: id
	            },
				config: {
	            	windowClass: 'x-window',
					width: 480
				}
	        }).then(function() {
	            $scope.treeReload();
	        });
	    }
		$scope.edit = function() {
	        var node = getTreeNode();
	        var id = node.id;
	        api.form({
	            title: '编辑分类',
	            template: require('./views/form_type.tpl.html'),
	            name: 'settings_type_edit',
	            data: {
	                id: id
	            },
				resolveWait: http.post({
	                name: 'settings_type',
	                params: {
	                    id: id
	                }
	            }),
	            config: {
	                windowClass: 'x-window',
	                width: 480
	            }
	        }).then(function() {
	            $scope.treeReload();
	        });
	    };
		$scope.remove = function() {
	        var node = getTreeNode();
	        var id = node.id;
	        dialog.confirm('确定要删除当前分类？').result.then(function(r) {
	            if (r) {
	                http.post({
	                    name: 'settings_type_remove',
	                    params: {
	                        id: id
	                    },
	                    success: function() {
	                        $scope.treeReload();
	                    }
	                })
	            }
	        });
	    };

		$scope.select = function(state) {
			if (state) {
				if ($state.is(state)) {
					$state.reload(state);
				} else {
					$state.go(state);
				}
			}
		}

		$scope.changeNode = function(node) {
			$scope.selectedTreeNode = node;

	        $scope.select('settings.list');
		}
	}
}