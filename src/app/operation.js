const operation = (api) => {
    let _json_index = 0, _temp_json, _temp_array;
    let _keys = ['name', 'value'];
    let service = {
        reset: function() {
            _json_index = 0;
            _temp_json = {};
            _temp_array = [];
            return this;
        },
        addJson: function() {
            _json_index++;
            let id = _json_index;
            _temp_json['key' + id] = 'value' + id;
            // _temp_array.length ++;
            _temp_array[_temp_array.length] = {};
            return this;
        },
        jsonArray: () => {
            return _temp_array;
        },
        getKey: (n) => {
            return _keys[n - 1];
        },
        setKeys: (keys) => {
            _keys = (keys || []).map((key) => {
                return key;
            });
        },
        getJsonValue: (key) => {
            return _temp_json[key];
        },
        removeJson: (key) => {
            if (typeof key == 'string') {
                let keys = service.jsonKeys();
                if (keys.length === 1) return;
                delete _temp_json[key];
            } else if (typeof key == 'number') {
                if (_temp_array.length === 1) return;
                _temp_array.splice(key, 1);
            }
        },
        jsonKeys: () => {
            return Object.keys(_temp_json);
        },
        setJsonData: function(tempJson, data) {
            if (!tempJson) {
                console.log('数据尚未初始化！');
                return;
            }
            let _data = angular.extend({}, data);
            let keys = Object.keys(_data);
            if (keys.length === 0) {
                service.addJson();
                return this;
            }
            for (let key in _data) {
                service.addJson();
                tempJson['key' + _json_index] = key;
                tempJson['value' + _json_index] = _data[key];
            }
            return this;
        },
        setArrayData: function(data) {
            service.reset();
            if (!data || data.length === 0) {
                service.addJson();
                return this;
            }
            data.forEach((json) => {
                service.addJson();
                let index = _temp_array.length - 1;
                _keys.forEach((key) => {
                    _temp_array[index][key] = json[key];
                });
            })
            return this;
        },
        getResultJson: (tempJson) => {
            if (!tempJson) {
                console.log('数据尚未初始化！');
                return;
            }
            let json = {};
            let keys = service.jsonKeys();
            keys.forEach((id) => {
                let key = tempJson[id];
                let value = tempJson[service.getJsonValue(id)];

                json[key] = value;
            });
            return json;
        },
        getResultArray: (tempJson) => {
            let result = [];
            _temp_array.forEach((json) => {
                if (json && Object.keys(json).length > 0) {
                    result.push(json);
                }
            });
            return result;
        },
        movePrev: (index) => {
            if (index > 0) {
                api.swap(_temp_array, index, index - 1);
            }
        },
        moveNext: (index) => {
            if (index < _temp_array.length - 1) {
                api.swap(_temp_array, index, index + 1);
            }
        }
    };
    return service;
}

operation.$inject = ['ui.api'];

export default operation;