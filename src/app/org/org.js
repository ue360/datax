const org = () => {
	let service = {
		moveAdd: function(cache, target) {
			cache = cache || [];
			let _scope = this, 
				data = _scope.roles || [];
			for (let i=0; i<data.length; i++) {
				let item = data[i];

				if (cache.indexOf(item.id) !== -1) {
					target.push(item);

					data.splice(i, 1);
					i--;
				}
			}
		},
		moveRemove: function(target, cache) {
			cache = cache || [];
			let _scope = this;
			_scope.roles = _scope.roles || [];
			for (let i=0; i<target.length; i++) {
				let item = target[i];

				if (cache.indexOf(item.id) !== -1) {
					_scope.roles.push(item);

					target.splice(i, 1);
					i--;
				}
			}
		}
	}
	return service;
}

export default org;