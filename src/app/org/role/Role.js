import {Inject} from 'plugins'
import config from 'config';

@Inject('$scope', '$state', 'ui.dialog', 'ui.api', 'ui.http')
export default class Role {
	constructor($scope, $state, dialog, api, http) {
		var q = $scope.q = {};

		var loadData = $scope.loadData = function(params) {
			api.gridLoad({
				name: 'org_roles',
				params: params,
				scope: $scope
			});
		};

		var query = $scope.query = function(resetPage) {
			if (resetPage === false || $scope.gridOptions.paginationCurrentPage == 1) {
				loadData(q);
			} else {
				api.gridReset(null, $scope);
			}
		}

		$scope.add = function() {
			api.form({
				title: '新建角色',
				template: require('./views/form.tpl.html'),
				name: 'org_role_add',
				beforeSettings: function() {
					var _scope = this;
					api.tree({
						scope: _scope,
						options: 'treeLibOptions',
						view: 'treeLib',
						type: 'checkbox',
						url: 'basic/module/fullModuleTree',
						params: {},
						paths: [],
						cascade: true
					});
					// api.tree({
					// 	scope: _scope,
					// 	options: 'treeDirOptions',
					// 	view: 'treeDir',
					// 	type: 'checkbox',
					// 	url: 'category/selectedCubeTree',
					// 	params: {},
					// 	paths: []
					// });
				},
				beforeSubmit: function(data) {
					var _scope = this;
					data.authIds = api.getTreeChecked(_scope.treeLib).map(function(node) {
						return node.id;
					});
					// data.cubeIds=api.getTreeChecked(_scope.treeDir).map(function(node){
					// 	return node.id;
					// });
				}
			}).then(function() {
				query();
			});
		}
		$scope.edit = function() {
			var id = api.getGridSelectedId($scope);
			api.form({
				title: '修改角色',
				template: require('./views/form.tpl.html'),
				name: 'org_role_edit',
				beforeSettings: function() {
					var _scope = this;
					api.tree({
						scope: _scope,
						options: 'treeLibOptions',
						view: 'treeLib',
						type: 'checkbox',
						url: 'basic/module/selectedModuleTree',
						params: {
							roleId: id
						},
						paths: [],
						cascade: true
					});
					// api.tree({
					// 	scope: _scope,
					// 	options: 'treeDirOptions',
					// 	view: 'treeDir',
					// 	type: 'checkbox',
					// 	url: 'category/selectedCubeTree',
					// 	params: {oid:id,type:1},
					// 	paths: []
					// });
				},
				resolveWait: {
					roles: function() {
						return http.post({
							name: 'org_role',
							params: {
								id: id
							}
						})
					}
				},
				resolveApplyData: ['roles'],
				beforeSubmit: function(data) {
					var _scope = this;
					data.authIds = api.getTreeChecked(_scope.treeLib).map(function(node) {
						return node.id;
					});
					// data.cubeIds=api.getTreeChecked(_scope.treeDir).map(function(node){
					// 	return node.id;
					// });
				}
			}).then(function() {
				query(false);
			});
		}
		$scope.remove = function() {
			var ids = api.getGridSelectedIds($scope);
			dialog.confirm('确定要删除已选择的角色？').result.then(function(r) {
				if (r) {
					http.post({
						name: 'org_role_remove',
						params: {
							ids: ids
						},
						success: function() {
							query(false);
						}
					})
				}
			});
		}

		$scope.gridOptions = {
			paginationCurrentPage: api.page.pageNumber,
			paginationPageSize: api.page.pageSize,
			useExternalPagination: true,
			columns: [{
				name: '序号',
				width: 50,
				cellTemplate: '<div class="x-grid-inner">{{(grid.options.paginationCurrentPage - 1) * grid.options.paginationPageSize + (index + 1)}}</div>'
			}, {
				name: '角色名',
				align: 'left',
				width: 150,
				field: 'name'
			}, {
				name: '描述',
				width: 150,
				field: 'description'
			}, {
				name: '类型',
				width: 100,
				field: 'grade'
			}, {
				name: '创建时间',
				width: 200,
				field: 'crtime'
			}],
			forceFit: false,
			onRegisterApi: function(api) {
				$scope.api = api;
				api.pagination && api.pagination.on.paginationChanged($scope, function(newPage, pageSize) {
					query(false);
				});
			}
		};

		$scope.hasSelected = function() {
			return api.hasGridSelected($scope);
		}
		$scope.hasSelectedRecords = function() {
			return api.hasGridSelectedRecords($scope);
		}

		query();
	}
}