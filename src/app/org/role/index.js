import {alias} from 'config'
import controller from './Role'

import template from './views/list.tpl.html';

alias({
    'org_roles': 'basic/role/roleList',
    'org_role': 'basic/role/read',
    'org_api_roles': 'basic/role/read',
    'org_role_valid': 'basic/role/checkRoleName',
    'org_role_add': 'basic/role/create',
    'org_role_edit': 'basic/role/updateRole',
    'org_role_remove': 'basic/role/batchDelete',
    'org_full_roles': 'basic/role/fullRoleList',
    'org_branch_roleList': 'basic/role/branchRoleList'
})

const router = ($stateProvider) => {
    $stateProvider.state('role', {
        url: '/role',
        views: {
            content: {
                template,
                controller,
                controllerAs: 'vm'
            }
        },
        parent: 'app',
        data: {
            name: 'config',
            title: '角色管理'
        }
    });
}
router.$inject = ['$stateProvider'];

export default angular.module('app.role', [])
    .config(router)
    .name;