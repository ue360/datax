import {alias} from 'config'
import controller from './User'

import template from './views/list.tpl.html';

alias({
    'org_users': 'basic/user/userList',
    'org_user': 'basic/user/read',
    'org_user_valid': 'basic/user/checkUserName',
    'org_user_edit': 'basic/user/updateUser',
    'org_user_add': 'basic/user/create',
    'org_user_remove': 'basic/user/batchDeleteUser',
    'org_user_reset': 'basic/user/updatePwd',
    'org_user_start': 'basic/user/batchEnable',
    'org_user_stop': 'basic/user/batchDisable',
    'org_remaining_roles': 'basic/user/remainingRoleList',
    'org_branch_userList': 'basic/user/branchUserList',
    'org_user_modify': 'basic/user/modifyUser',
    'org_user_batchInfo': 'basic/user/batchUserInfo'
})

const router = ($stateProvider) => {
    $stateProvider.state('user', {
        url: '/user',
        views: {
            content: {
                template,
                controller,
                controllerAs: 'vm'
            }
        },
        parent: 'app',
        data: {
            name: 'config',
            title: '用户管理'
        }
    });
}
router.$inject = ['$stateProvider'];

export default angular.module('app.user', [])
    .config(router)
    .name;