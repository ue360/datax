import {Inject} from 'plugins'

@Inject('$scope', '$state', 'ui.dialog', 'ui.http', 'ui.api', 'OrgService')
export default class User {
	constructor($scope, $state, dialog, http, api, appService) {
		var q = $scope.q = {};

		var loadData = $scope.loadData = function(params) {
				api.gridLoad({
					name: 'org_users', 
					params: params,
					scope: $scope,
	                success: function(data, options) {
	                    var _scope = this;
	                    _scope.fields = data.fields;
	                }
				});
			};

	    var query = $scope.query = function(resetPage) {
			if (resetPage === false || $scope.gridOptions.paginationCurrentPage == 1) {
				loadData(q);
			} else {
				api.gridReset(null, $scope);
			}
		}

		$scope.add = function() {
			api.form({
				title: '新建用户', 
				template: require('./views/form.tpl.html'),
				name: 'org_user_add',
				beforeSettings: function() {
					var _scope = this;
					api.tree({
						scope: _scope,
						type: 'checkbox',
						url: 'basic/branch/treeBranch1',
						params: {},
						paths: []
					});
				},
				scope: {
					moveAdd: appService.moveAdd,
					moveRemove: appService.moveRemove
				},
				resolveWait: {
					roles: function() {
						return http.post('org_full_roles');
					}
				},
				resolveApplyScope: ['roles'],
				beforeSubmit: function(data) {
					var _scope=this;
					data.roleIds = (data.roles || []).map(function(role) {
						return role.id;
					});
					data.roles = null;
					delete data.roles;
					
					data.branchIds=api.getTreeChecked(_scope.tree).map(function(node){
						return node.id;
					});
				}
			}).then(function() {
				query();
			});
		}
		$scope.edit = function() {
			var id = api.getGridSelectedId($scope);

			api.form({
				title: '修改用户', 
				template: require('./views/form.tpl.html'),
				name: 'org_user_edit',
				beforeSettings: function() {
					var _scope = this;
					api.tree({
						scope: _scope,
						type: 'checkbox',
						url: 'basic/branch/selectedBranchTree',
						params: {useId:id},
						paths: []
					});
				},
				scope: {
					moveAdd: appService.moveAdd,
					moveRemove: appService.moveRemove
				},
				resolveWait: {
					roles: function() {
						return http.post({
							name:'org_remaining_roles',
							params:{
								id:id
							}
						});
					},
					items: function() {
						return http.post({
							name: 'org_user',
							params: {
								id: id
							}
						})
					}
				},
				resolveApplyScope: ['roles'],
				resolveApplyData: ['items'],
				beforeSubmit: function(data) {
					var _scope=this;
					data.roleIds = (data.roles || []).map(function(role) {
						return role.id;
					});
					data.branchIds=api.getTreeChecked(_scope.tree).map(function(node){
						return node.id;
					});
				}
			}).then(function() {
				query(false);
			});
		}
		$scope.remove = function() {
			var ids = api.getGridSelectedIds($scope);
			dialog.confirm('确定要删除已选择的用户？').result.then(function(r) {
				if (r) {
					http.post({
						name: 'org_user_remove',
						params: {
							ids: ids
						},
						success: function() {
							query(false);
						}
					})
				}
			});
		}

		$scope.reset = function() {
			var ids = api.getGridSelectedIds($scope);
			api.form({
				title: '密码重置',
				template: require('./views/form_reset.tpl.html'),
				name: 'org_user_reset',
				config: {
					windowClass: 'x-window',
					width: 480
				},
				data:{
					ids:ids
				}
			});
		}
		
		$scope.start = function() {
			var ids = api.getGridSelectedIds($scope);
			dialog.confirm('确定要启用已选择的用户？').result.then(function(r) {
				if (r) {
					http.post({
						name: 'org_user_start',
						params: {
							ids:ids
						},
						success: function() {
							query(false);
						}
					})
				}
			});
		}
		
		$scope.stop = function() {
			var ids = api.getGridSelectedIds($scope);
			dialog.confirm('确定要禁用已选择的用户？').result.then(function(r) {
				if (r) {
					http.post({
						name: 'org_user_stop',
						params: {
							ids:ids
						},
						success: function() {
							query(false);
						}
					})
				}
			});
		}

		$scope.gridOptions = {
			paginationCurrentPage: api.page.pageNumber,
			paginationPageSize: api.page.pageSize,
			useExternalPagination: true,
			columns: [{
				name: '序号',
				width: 50,
				cellTemplate: '<div class="x-grid-inner">{{(grid.options.paginationCurrentPage - 1) * grid.options.paginationPageSize + (index + 1)}}</div>'
			}, {
				name: '用户名',
				align: 'left',
				width: 150,
				field: 'username'
			}, {
				name: '真实姓名',
				width: 150,
				field: 'realname'
			}, {
				name: '当前状态',
				width: 100,
				field: 'status',
	            cellTemplate: '<div class="x-grid-inner" ng-bind-html="grid.appScope.mapping(grid, row, col, \'status\')"></div>'
			}, {
				name: '创建时间',
				width: 200,
				field: 'crtime'
			}],
			forceFit: false,
			onRegisterApi: function(api) {
				$scope.api = api;
				api.pagination && api.pagination.on.paginationChanged($scope, function(newPage, pageSize) {
					query(false);
				});
			}
		};
		
		$scope.mapping = function(grid, row, col, field) {
			return api.mapping({
				grid: grid,
				row: row,
				col: col,
	            field: field,
				scope: $scope
			});
		}

		$scope.hasSelected = function() {
			return api.hasGridSelected($scope) && api.getGridSelectedRecord($scope).status != -1;
		}
		$scope.hasSelectedRecords = function() {
			return api.hasGridSelectedRecords($scope) && api.getGridSelectedRecords($scope).filter(function(r) {
					return r.status == -1;
	            }).length == 0;
		}

		query();
	}
}