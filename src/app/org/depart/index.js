import {alias} from 'config';
import * as app from './components';

import DepartService from './depart';

alias({
    'org_departs': 'api/grid.json',
    'org_depart_add': 'basic/branch/create',
    'org_depart': 'basic/branch/read',
    'org_depart_list': 'basic/branch/treeBranch',
    'org_depart_edit': 'basic/branch/updateBranch',
    'org_depart_remove': 'basic/branch/batchDelete',
    'org_depart_remaining_roles': 'basic/branch/remainingRoleList',
    'org_depart_remaining_users': 'basic/branch/remainingUserList',
    'org_depart_surplus_role': 'basic/branch/surplusRoleList',
    'org_depart_roleAddAndRemove': 'basic/branch/roleAddAndRemove',
    'org_depart_userAddAndRemove': 'basic/branch/userAddAndRemove',
    'org_depart_cubeAddAndRemove': 'basic/branch/cubeAddAndRemove',
    'org_depart_name_valid': 'basic/branch/checkBranchName',
    'org_depart_code_valid': 'basic/branch/checkBranchCode',
    'org_depart_cubeList': 'basic/branch/branchCubeList'
})

const router = ($stateProvider) => {
    $stateProvider
        .state('depart', {
            abstract: true,
            views: {
                "content": {
                    template: require('./views/layout.tpl.html'),
                    controller: app.Depart,
                }
            },
            data: {
                name: 'config',
                title: '部门管理',
                welcome: 'depart.info',
                tabs: [{
                    state: 'depart.manager.user',
                    active: true,
                    name: '用户管理'
                }/*, {
                    state: 'depart.manager.model',
                    name: '资源目录管理'
                }*/, {
                    state: 'depart.manager.role',
                    name: '角色管理'
                }]
            },
            parent: 'app'
        })
        .state('depart.list', {
            url: '/depart',
            views: {
                "primary": {
                    template: require('./views/list.tpl.html'),
                    controller: app.DepartList
                }
            },
            data: {
                title: '部门列表'
            }
        })
        .state('depart.info', {
            url: '/depart/info',
            views: {
                "primary": {
                    template: require('templates/welcome.tpl.html')
                }
            },
            data: {
                title: '欢迎页'
            }
        })
        .state('depart.manager', {
            url: '/depart',
            abstract: true,
            views: {
                "primary": {
                    template: require('./views/list_other.tpl.html'),
                    controller: app.DepartManager
                }
            }
        })
        .state('depart.manager.user', {
            url: '/user',
            views: {
                "tabpanel": {
                    template: require('./views/list_user.tpl.html'),
                    controller: app.DepartUserList
                }
            },
            data: {
                title: '用户列表'
            }
        })
        .state('depart.manager.model', {
            url: '/model',
            views: {
                "tabpanel": {
                    template: require('./views/list_model.tpl.html'),
                    controller: app.DepartModelList
                }
            },
            data: {
                title: '资源目录列表'
            }
        })
        .state('depart.manager.role', {
            url: '/role',
            views: {
                "tabpanel": {
                    template: require('./views/list_role.tpl.html'),
                    controller: app.DepartRoleList
                }
            },
            data: {
                title: '角色列表'
            }
        });
}
router.$inject = ['$stateProvider'];

export default angular.module('app.depart', [])
    .config(router)
    .factory('DepartService', DepartService)
    .name;