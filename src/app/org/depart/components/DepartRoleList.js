import {Inject} from 'plugins'
import config from 'config';

@Inject('$scope', '$state', 'ui.dialog', 'ui.http', 'ui.api', 'DepartService')
export class DepartRoleList {
	constructor($scope, $state, dialog, http, api, service) {
		var appScope = $scope.$parent.$parent;

		function getTreeNode() {
			return appScope.getTreeNode();
		}

		appScope.changeState();

		var q = $scope.q = {};

		var loadData = $scope.loadData = function(params) {
			var node = getTreeNode();
			if (!node) return;
			params = params || {};
			angular.extend(params, {
				id: node.id
			})
			api.gridLoad({
				name: 'org_branch_roleList',
				params: params,
				scope: $scope
			});
		};

		var query = $scope.query = function(resetPage) {
			if (resetPage === false || $scope.gridOptions.paginationCurrentPage == 1) {
				loadData(q);
			} else {
				api.gridReset(null, $scope);
			}
		}

		$scope.addDepart = function() {
			service.add(appScope);
		}

		$scope.add = function() {
			var node = getTreeNode();
			api.form({
				title: '新建角色',
				template: require('../../role/views/form.tpl.html'),
				name: 'org_role_add',
				data: {

				},
				beforeSettings: function() {
					var _scope = this;
					api.tree({
						scope: _scope,
						options: 'treeLibOptions',
						view: 'treeLib',
						type: 'checkbox',
						url: 'basic/module/fullModuleTree',
						params: {},
						paths: [],
						cascade: true,
	                    cache: true
					});
					// api.tree({
					// 	scope: _scope,
					// 	options: 'treeDirOptions',
					// 	view: 'treeDir',
					// 	type: 'checkbox',
					// 	url: 'category/selectedCubeTree',
					// 	params: {},
					// 	paths: []
					// });
				},
				beforeSubmit: function(data) {
					var _scope = this;
					data.authIds = api.getTreeChecked(_scope.treeLib).map(function(node) {
						return node.id;
					});
					// data.cubeIds = api.getTreeChecked(_scope.treeDir).map(function(node) {
					// 	return node.id;
					// });
				}
			}).then(function() {
				query();
			});
		}
		$scope.edit = function() {
			var node = getTreeNode();
			var id = api.getGridSelectedId($scope);
			api.form({
				title: '修改角色',
				template: require('../../role/views/form.tpl.html'),
				name: 'org_role_edit',
				data: {
					id: node.id
				},
				beforeSettings: function() {
					var _scope = this;
					api.tree({
						scope: _scope,
						options: 'treeLibOptions',
						view: 'treeLib',
						type: 'checkbox',
						url: 'basic/module/selectedModuleTree',
						params: {roleId:id},
						paths: [],
						cascade: true,
	                    cache: true
					});
					// api.tree({
					// 	scope: _scope,
					// 	options: 'treeDirOptions',
					// 	view: 'treeDir',
					// 	type: 'checkbox',
					// 	url: 'category/selectedCubeTree',
					// 	params: {oid:id,type:1},
					// 	paths: []
					// });
				},
				resolveWait: http.post({
					name: 'org_role',
					params: {
						id: api.getGridSelectedId($scope)
					}
				}),
				resolveApplyData: ['roles'],
				beforeSubmit: function(data) {
					var _scope = this;
					data.authIds = api.getTreeChecked(_scope.treeLib).map(function(node) {
						return node.id;
					});
					// data.cubeIds = api.getTreeChecked(_scope.treeDir).map(function(node) {
					// 	return node.id;
					// });
				}
			}).then(function() {
				query(false);
			});
		}
		$scope.remove = function() {
			var node = getTreeNode();
			var id = node.id;
			var ids = api.getGridSelectedIds($scope);
			dialog.confirm('确定要移除已选择的角色？').result.then(function(r) {
				if (r) {
					http.post({
						name: 'org_depart_roleAddAndRemove',
						params: {
							id: id,
							ids: ids,
							type: 2,
						},
						success: function() {
							query(false);
						}
					})
				}
			});
		}
		$scope.import = function() {
			var node = getTreeNode();
			var id = node.id;

			var loadList = function(scope, params) {
				scope = scope || this;
				params = params || {};
				angular.extend(params, {
					id: id
				});
				return api.gridLoad({
					name: 'org_depart_surplus_role',
					params: params,
					scope: scope
				});
			};
			api.form({
				title: '添加角色',
				template: require('../views/form_import.tpl.html'),
				scope: {
					q: {},
					gridOptions: {
						paginationCurrentPage: api.page.pageNumber,
						paginationPageSize: api.page.pageSize,
						useExternalPagination: true,
						columns: [{
							name: '序号',
							width: 50,
							cellTemplate: '<div class="x-grid-inner">{{(grid.options.paginationCurrentPage - 1) * grid.options.paginationPageSize + (index + 1)}}</div>'
						}, {
							name: '角色名',
							align: 'left',
							width: 150,
							field: 'name'
						}, {
							name: '创建用户',
							width: 150,
							field: 'cruser'
						}, {
							name: '描述',
							width: 100,
							field: 'description'
						}, {
							name: '创建时间',
							width: 200,
							field: 'crtime'
						}],
						data: [],
						onRegisterApi: function(api, grid) {
							var _scope = grid.appScope;
							_scope.api = api;

							api.pagination && api.pagination.on.paginationChanged(_scope, function(newPage, pageSize) {
								_scope.query(false);
							});
						}
					},
					query: function(resetPage) {
						var _scope = this;
						if (resetPage === false || _scope.gridOptions.paginationCurrentPage == 1) {
							loadList(_scope, _scope.q);
						} else {
							api.gridReset(null, _scope);
						}
					}
				},
				name: 'org_depart_roleAddAndRemove',
				data: {
					id: id
				},
				beforeSubmit: function(data, defered) {
					var ids = api.getGridSelectedIds(this);
					if (ids.length === 0) {
						dialog.alert('请选择需要添加的角色！');
						return false;
					}
					data.ids = ids;
					data.type = 1;
				},
				resolveApply: false,
				resolveWait: function() {
					var _scope = this;
					return loadList(_scope);
				},
				config: {
					size: 680
				}
			}).then(function() {
				query();
			});
		}

		$scope.gridOptions = {
			paginationCurrentPage: api.page.pageNumber,
			paginationPageSize: api.page.pageSize,
			useExternalPagination: true,
			columns: [{
				name: '序号',
				width: 50,
				cellTemplate: '<div class="x-grid-inner">{{(grid.options.paginationCurrentPage - 1) * grid.options.paginationPageSize + (index + 1)}}</div>'
			}, {
				name: '角色名',
				align: 'left',
				width: 150,
				field: 'name'
			}, {
				name: '创建用户',
				width: 150,
				field: 'cruser'
			}, {
				name: '描述',
				width: 100,
				field: 'description'
			}, {
				name: '创建时间',
				width: 200,
				field: 'crtime'
			}],
			forceFit: false,
			onRegisterApi: function(api) {
				$scope.api = api;
				api.pagination && api.pagination.on.paginationChanged($scope, function(newPage, pageSize) {
					query(false);
				});
			}
		};

		$scope.hasSelected = function() {
			return api.hasGridSelected($scope);
		}
		$scope.hasSelectedRecords = function() {
			return api.hasGridSelectedRecords($scope);
		}

		query();
	}
}