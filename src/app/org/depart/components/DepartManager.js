import {Inject} from 'plugins'
import config from 'config';

@Inject('$scope')
export class DepartManager {
	constructor($scope) {
		var appScope = $scope.$parent;
		var tabs = $scope.tabs = appScope.tabs;

		$scope.select = function(index) {
			if (appScope.activeIndex == index) return;
			var tab = tabs[index];
			appScope.activeIndex = index;
			appScope.select(tab.state);
		}
	}
}