import {Inject} from 'plugins'
import config from 'config';

@Inject('$scope', '$state', 'ui.dialog', 'ui.http', 'ui.api', 'DepartService')
export class DepartList {
	constructor($scope, $state, dialog, http, api, service) {
		var appScope = $scope.$parent;

		function getTreeNode() {
			return appScope.getTreeNode();
		}
		var _node = getTreeNode();
		if (!_node) {
			appScope.select(appScope.welcome);
			return;
		} else {
			var tree = appScope.tree;
			var root = tree.dataSource.at(0);
			if (_node.id != root.id) {
				api.treeSelect(appScope.tree, root.id);
				return;
			}
		}

		var getTreeNodes = function() {
			return api.getTreeListNodes($scope)
		}
		var getTreeNodeIds = function() {
			return api.getTreeListNodeIds($scope);
		}

		$scope.treeReload = function() {
			return api.treeListReload(true, $scope);
		}

		var dataSource = new kendo.data.TreeListDataSource({
			transport: {
				read: {
					url: config.apiRootUrl + "basic/branch/treeBranch1",
					dataType: 'json'
				}
			},
			schema: api.schema
		});

		// $scope.toggleAll = function(e) {
		// 	var rows = dataSource.view();
		// 	var checked = e.target.checked;
		// 	for (var i = 0; i < rows.length; i++) {
		// 		rows[i].set("checked", checked);
		// 	}
		// }

		$scope.checkSelect = function(data) {
			var view = $scope.tree;
			var row = view.itemFor(data);
			if (data.checked) {
				view.select(row);
			} else if (row.hasClass('k-state-selected')) {
				view.selectable._unselect(row);
				// view.clearSelection();
				changeState();
			}
		}

		$scope.hasSelected = false;
		$scope.hasSelectedRecords = false;

		function changeState() {
			var nodes = getTreeNodes();

			$scope.hasSelected = false;
			$scope.hasSelectedRecords = false;
			if (nodes) {
				if (nodes.length === 1) {
					$scope.hasSelected = true;
					$scope.hasSelectedRecords = true;
				} else if (nodes.length > 0) {
					$scope.hasSelected = false;
					$scope.hasSelectedRecords = true;
				}
			}
		}

		$scope.add = function() {
			service.add(appScope);
		}
		$scope.edit = function(id) {
			id = id == null ? getTreeNodeIds()[0] : id;
			service.edit(appScope, id);
		}
		$scope.remove = function() {
			var ids = getTreeNodeIds();
			service.remove(appScope, ids);
		}

		$scope.treeListOptions = {
			dataSource: dataSource,
			sortable: true,
			selectable: 'multiple, row',
			editable: false,
			resizable: true,
			columns: [{
				// headerTemplate: "<input type='checkbox' ng-click='toggleAll($event)' />",
				template: "<input type='checkbox' ng-model='dataItem.checked'  ng-click='checkSelect(dataItem)' />",
				width: 32
			}, {
				field: "text",
				expandable: true,
				title: "部门名称",
				template: '<span class="k-node"><i class="fa fa-folder-open"></i> {{dataItem.text}}</span>'
			}, {
				field: "author",
				title: "创建人"
			}, {
				field: "date",
				title: "创建时间"
			}, {
				title: "操作",
				command: [{
					name: 'modify',
					className: 'btn-edit',
					text: '修改',
					click: function(e) {
						var tr = $(e.target).closest("tr");
						var data = this.dataItem(tr);

						$scope.edit(data.id);
					}
				}]
			}],
			change: function(e) {
				changeState();

				var view = this;

				this.content.find("td:first-child :checked").each(function(index, el) {
					var node = view.dataItem(el);
					node.checked = false;
				});

				getTreeNodes().forEach(function(node) {
					node.checked = true;
				});

				if (!$scope.$$phase) {
					$scope.$digest();
				}
			}
		};
	}
}