import {Inject} from 'plugins'
import config from 'config';

@Inject('$scope', '$state', 'ui.dialog', 'ui.http', 'ui.api', 'DepartService')
export class DepartModelList {
	constructor($scope, $state, dialog, http, api, service) {
		var appScope = $scope.$parent.$parent;

		function getTreeNode() {
			return appScope.getTreeNode();
		}

		appScope.changeState();

		var q = $scope.q = {};

		var loadData = $scope.loadData = function(params) {
			var node = getTreeNode();
			if (!node) return;
			params = params || {};
			angular.extend(params, {
				id: node.id
			})
			api.gridLoad({
				name: 'org_depart_cubeList',
				params: params,
				scope: $scope
			});
		};

		var query = $scope.query = function(resetPage) {
			if (resetPage === false || $scope.gridOptions.paginationCurrentPage == 1) {
				loadData(q);
			} else {
				api.gridReset(null, $scope);
			}
		}

		$scope.addDepart = function() {
			service.add(appScope);
		}

		$scope.add = function() {
			var node = getTreeNode();
			api.form({
				title: '新建资源目录',
				template: require('../../../meta/views/form_extend_model.tpl.html'),
				name: 'org_model_add',
				data: {
					id: node.id
				},
				beforeSettings: function() {
					var _scope = this;
					api.tree({
						scope: _scope,
						type: 'radio',
						url: 'api/tree.json',
						params: {},
						paths: []
					});
				},
				config: {
					width: 960
				}
			}).then(function() {
				query();
			});
		}
		$scope.edit = function() {
			var node = getTreeNode();
			api.form({
				title: '修改资源目录',
				template: require('../../../meta/views/form_extend_model.tpl.html'),
				name: 'org_model_edit',
				data: {
					id: node.id
				},
				beforeSettings: function() {
					var _scope = this;
					api.tree({
						scope: _scope,
						type: 'radio',
						url: 'api/tree.json',
						params: {},
						paths: []
					});
				},
				resolveWait: http.post({
					name: 'org_model',
					params: {
						id: api.getGridSelectedId($scope)
					}
				}),
				config: {
					width: 960
				}
			}).then(function() {
				query(false);
			});
		}
		$scope.remove = function() {
			var node = getTreeNode();
			var id = node.id;
			var ids = api.getGridSelectedIds($scope);
			dialog.confirm('确定要移除已选择的资源目录？').result.then(function(r) {
				if (r) {
					http.post({
						name: 'org_depart_cubeAddAndRemove',
						params: {
							id: id,
							ids: ids,
							type: 2
						},
						success: function() {
							query(false);
						}
					})
				}
			});
		}

		$scope.import = function() {
			var node = getTreeNode();
			var id = node.id;
			api.select({
				title: '选择资源目录',
				type: 'checkbox',
				postTree: 'category/selectedCubeTree',
				treeData: {
					oid: id,
					type: 2
				},
				postName: 'org_depart_cubeAddAndRemove',
				postParams: {
					id: id,
					type: 1
				}
			}).then(function() {
				query();
			});
		}

		$scope.gridOptions = {
			paginationCurrentPage: api.page.pageNumber,
			paginationPageSize: api.page.pageSize,
			useExternalPagination: true,
			columns: [{
					name: '序号',
					width: 50,
					cellTemplate: '<div class="x-grid-inner">{{(grid.options.paginationCurrentPage - 1) * grid.options.paginationPageSize + (index + 1)}}</div>'
				}, {
					name: '目录名称',
					align: 'left',
					width: 150,
					field: 'cubeNameCn'
				},
				/* {
							name: '所属分类',
							width: 150,
							field: 'category.name'
						},*/
				{
					name: '创建时间',
					width: 300,
					field: 'crtime'
				}
			],
			forceFit: false,
			onRegisterApi: function(api) {
				$scope.api = api;
				api.pagination && api.pagination.on.paginationChanged($scope, function(newPage, pageSize) {
					query(false);
				});
			}
		};

		$scope.hasSelected = function() {
			return api.hasGridSelected($scope);
		}
		$scope.hasSelectedRecords = function() {
			return api.hasGridSelectedRecords($scope);
		}

		query();
	}
}