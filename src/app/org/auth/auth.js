const auth = () => {
    var s = '@@';

    function setId(id, type) {
        if (typeof id == 'object') {
            var cfg = id;
            id = cfg[service.param.id],
            type = cfg[service.param.type];
        }
        return type + s + id;
    }

    function getId(id) {
        return id.toString().split(s)[1] || id;
    }

    function getIdAndType(id) {
        var rs = id.split(s);
        if (id.length < 2) {
            throw ('参数异常!');
        }
        return {
            id: rs[1],
            type: rs[0]
        }
    }
    var service = {
        param: {
            id: 'oprId',
            type: 'objType',
            auth: 'auth',
            nodeid: 'rightId'
        },
        setId: setId,
        getId: getId,
        getIdAndType: getIdAndType,
        // sequence: function(r) {
        //  r.data = (r.data || []).map(function(row) {
        //      row.id = row.type == null ? row.id : setId(row.id, row.type);
        //      return row;
        //  });
        // },
        setAuth: function(grid, row, index, authorization) {
            var auth = grid.getRowValue(row, 'auth') || {};

            if (authorization == null) {
                auth[index] = auth[index] == null ? true : !auth[index];
            } else {
                auth[index] = authorization;
            }
            var data = {
                auth: auth
            };
            grid.setData(row, data);
        },
        format: function(str) {
            var v = '0';
            return str ? (str.toString().replace(/^(0+)?/g, '') || v) : v;
        },
        getAuths: function(data) {
            return (data || []).map(function(row) {
                var auth = row.auth;
                if (typeof auth == 'object') {
                    var keys = Object.keys(auth);
                    var auths = keys.sort(function(x, y) {
                        return x < y;
                    });
                    var count = auths[0];
                    if (count) {
                        var _auths = [];
                        for (var i = 0; i < count; i++) {
                            _auths[i] = '0';
                        }
                        keys.forEach(function(index) {
                            if (auth[index] === true) {
                                _auths[index - 1] = '1';
                            }
                        });
                        auth = _auths.reverse().join('');
                    }
                }
                auth = service.format(auth);
                var result = {};
                var param = service.param;
                var id = param.id;
                var type = param.type;

                // result[id] = getId(row[id]);
                result[id] = row[id];
                result[type] = row[type];
                result[param.auth] = auth;
                result[param.nodeid] = row[param.nodeid];
                return result;
            });
        },
        equals: function(nowData, oldData) {
            if (nowData.length != oldData.length) {
                return false;
            }
            var id = service.param.id;
            var type = service.param.type;

            nowData.sort(function(x, y) {
                return setId(x) > setId(y);
            });
            oldData.sort(function(x, y) {
                return setId(x) > setId(y);
            });
            var name = service.param.auth;
            for (var i = 0, l = nowData.length; i < l; i++) {
                var row = nowData[i];
                var _row = oldData[i];
                if (setId(row) != setId(_row)) {
                    return false;
                }
                if (row[name] != _row[name]) {
                    return false;
                }
            }
            return true;
        },
        authorization: function(columns, data) {
            var name = service.param.auth;
            (data || []).forEach(function(row) {
                var auth = row[name];
                if (auth && typeof auth != 'object') {
                    var _auth = row[name] = {};
                    auth = service.format(auth).split('').reverse().join('');

                    (columns || []).forEach(function(column) {
                        var index = column.index;
                        if (index != null) {
                            index = parseInt(index, 10);
                            if (auth.length >= index && auth.charAt(index - 1) == 1) {
                                _auth[index] = true;
                            }
                        }
                    });
                }
            });
        }
    }
    return service;
}

export default auth;