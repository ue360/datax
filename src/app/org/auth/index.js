import {alias} from 'config';
import * as app from './components';

import AuthService from './auth';

alias({
    'auth_models': 'basic/purview/authModels.json',
    'auth_remove': 'basic/purview/batchDeletePurview',
    'auth_save': 'basic/purview/addPurviewList'
})

const router = ($stateProvider) => {
    $stateProvider
        .state('auth', {
            abstract: true,
            views: {
                "content": {
                    controller: app.Auth,
                    template: require('templates/tab_layout.tpl.html')
                }
            },
            parent: 'app',
            data: {
                name: 'config',
                title: '授权管理',
                welcome: 'auth.layout.info',
                tabs: [{
                    state: 'auth.layout.models',
                    name: '资源目录授权'
                }]
            }
        })
        .state('auth.layout', {
            abstract: true,
            views: {
                "tabpanel": {
                    controller: app.AuthLayout,
                    template: require('templates/layout.tpl.html')
                }
            }
        })
        .state('auth.layout.info', {
            url: '/auth/info',
            views: {
                "primary": {
                    template: require('templates/welcome.tpl.html')
                }
            },
            data: {
                title: '欢迎页'
            }
        })
        .state('auth.layout.models', {
            url: '/auth/models',
            views: {
                "primary": {
                    controller: app.AuthModels,
                    template: require('./views/list.tpl.html')
                }
            },
            data: {
                title: '资源目录授权'
            }
        });
}
router.$inject = ['$stateProvider'];

export default angular.module('app.auth', [])
    .config(router)
    .factory('AuthService', AuthService)
    .name;