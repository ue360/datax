import {
	Inject
} from 'plugins'
import config from 'config';

@Inject('$scope', '$state', 'ui.dialog', 'ui.http', 'ui.api')
export class AuthLayout {
	constructor($scope, $state, dialog, http, api) {
		api.tree({
			scope: $scope,
			type: 'tree',
			url: 'category/treeCategory',
			params: {

			}
		});

		$scope.treeReload = function() {
			api.treeReload.apply($scope, arguments);
		}

		var getTreeNode = $scope.getTreeNode = function() {
			return $scope.selectedTreeNode;
		}

		$scope.changeNode = function(node) {
			$scope.selectedTreeNode = node;

			$scope.$parent.select($scope.$parent.activeIndex);
		}
	}
}