import OrgService from './org';

import depart from './depart';
import user from './user';
import role from './role';
import auth from './auth';

export default angular.module('app.org', [
		depart,
		user,
		role,
		auth
	])
	.factory('OrgService', OrgService)
    .name;