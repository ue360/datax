import {AppConfig, AppSystem, interceptor} from 'config';

import app from './app';

import template from './app.tpl.html';
import navigation from './navigation';

import ConfigApiService from './operation'

import './style.less'

const user = (User) => {
    return User.request();
}
user.$inject = ['User'];

const router = ($stateProvider, $urlRouterProvider) => {
	$urlRouterProvider.otherwise('/login');
	$stateProvider.state('app', {
		abstract: true,
		views: {
            root: {
                template
            }
        },
        resolve: {
            user
        }
	});
}
router.$inject = ['$stateProvider', '$urlRouterProvider'];

export default angular
	.module('app', [
		'ngSanitize',
        'ngMessages',
        'ngAnimate',
        'ngFileUpload',
        
        'ui.codemirror',
        
        'ui.bootstrap',
        'ui.router',
        'ui.grid',
        'app.plugins',
        'kendo.directives',
        'infinite-scroll',

        ...app
	])
	.config(router)
	.config(interceptor)
	.constant('APP_CONFIG', AppConfig)
	.directive('navigation', navigation)
	.run(AppSystem)
	.factory('ConfigApiService', ConfigApiService)
	.name;
