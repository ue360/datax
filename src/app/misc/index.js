import {alias} from 'config';

alias({
	'gallery': 'gallery.json',
    'gallery_group_add': 'gallery-group-add.json',
	'pic_gallery': 'pic/gallery?type=1',
    'file_gallery': 'pic/gallery?type=2',
    'pic_group_add': 'pic/picGroupAdd',
    'pic_group_edit': 'pic/picGroupEdit',
    'pic_group_remove': 'pic/picGroupRemove',
    'pic_item_remove': 'pic/picItemRemove',
    'file_group_add': 'pic/fileGroupAdd'
});
const router = ($stateProvider) => {
	$stateProvider
		.state('404', {
			url: '/404',
			views: {
				content: {
					template: require('./views/404.tpl.html')
				}
			},
			data: {
	            title: '404错误'
	        },
	        parent: 'app'
		})
		.state('500', {
			url: '/500',
			views: {
				content: {
					template: require('./views/500.tpl.html')
				}
			},
			data: {
	            title: '500错误'
	        },
	        parent: 'app'
		})
		.state('forbidden', {
			url: '/forbidden',
			views: {
				content: {
					template: require('./views/forbidden.tpl.html')
				}
			},
			data: {
	            title: '没有权限'
	        },
	        parent: 'app'
		});
}
router.$inject = ['$stateProvider'];

export default angular.module('app.misc', [])
	.config(router)
	.name;
