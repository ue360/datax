import {Inject} from 'plugins'
import config from 'config';

@Inject('$scope', '$sce', '$state', 'ui.http', 'ui.dialog', 'ui.api', 'DepartService')
export default class Notice {
	constructor($scope, $sce, $state, http, dialog, api, DepartService) {
		var q = $scope.q = {};

		var loadData = $scope.loadData = function(params) {
				api.gridLoad({
					name: 'notice_list', 
					params: params,
					scope: $scope
				});
			};

	    var query = $scope.query = function(resetPage) {
			if (resetPage === false || $scope.gridOptions.paginationCurrentPage == 1) {
				loadData(q);
			} else {
				api.gridReset(null, $scope);
			}
		}

		$scope.add = function() {
			api.form({
				title: '发布公告', 
				template: require('./views/form.tpl.html'), 
				name: 'notice_add',
				data: {
					'type': 1
				},
				scope: {
					selectList: function(field) {
						var _scope = this;
						DepartService.select().then(function(users) {
							var ids = [];
							// 显示名
							_scope[field] = (users || []).map(function(r) {
								ids.push(r.id);
								return r.username;
							}).join(',');
							// 入库id集合
							_scope.data[field] = ids.join(',');
						});
					},
					selectTree: function(field) {
						var _scope = this;
						var data = _scope.data;
						api.select({
							title: '选择部门',
	                        paths: _scope.paths,
							postTree: 'basic/branch/treeBranch'
						}).then(function(nodes) {
	                        var ids = [];
	                        var names = [];

	                        _scope.paths = (nodes || []).map(function(node) {
	                            var _path = [];
	                            ids.push(node.id);
	                            names.push(node.text);
	                            while (node) {
	                                _path.push(node.id);
	                                node = node.parentNode();
	                            }
	                            _path.reverse();
	                            return _path;
	                        });
	                        // 显示名
	                        _scope[field] = names;
	                        // 入库id集合
	                        data[field] = ids.join(',');
						});
					}
				},
	            config: {
	                resolve: {
	                    scripts: ['lazyScript', function(lazyScript) {
	                        return lazyScript.register('assets/js/editor/ckeditor.js');
	                    }]
	                }
	            }
			}).then(function() {
				query();
			});
		}
		$scope.edit = function() {
			var id = api.getGridSelectedId($scope);

			api.form({
				title: '修改公告', 
				template: require('./views/form.tpl.html'), 
				name: 'notice_edit',
				resolveWait: http.post({
					name: 'notice_item',
					params: {
						id: id
					}
				}),
				resolveAfter: function(data) {
					var _scope = this;
					_scope.departs = data.branchName;
				},
				scope: {
					selectList: function(field) {
						var _scope = this;
						DepartService.select().then(function(users) {
							var ids = [];
							// 显示名
							_scope[field] = (users || []).map(function(r) {
								ids.push(r.id);
								return r.username;
							}).join(',');
							// 入库id集合
							_scope.data[field] = ids.join(',');
						});
					},
					selectTree: function(field) {
						var _scope = this;
	                    var data = _scope.data;
						api.select({
							title: '选择部门',
							paths: _scope.paths || (data.depts || []).map(function(item) {
	                            return item.path || '';
	                        }),
							postTree: 'basic/branch/treeBranch'
						}).then(function(nodes) {
							var ids = [];
							var names = [];

	                        _scope.paths = (nodes || []).map(function(node) {
	                            var _path = [];
	                            ids.push(node.id);
	                            names.push(node.text);
	                            while (node) {
	                                _path.push(node.id);
	                                node = node.parentNode();
	                            }
	                            _path.reverse();
	                            return _path;
	                        });
	                        // 显示名
	                        _scope[field] = names;
	                        // 入库id集合
	                        data[field] = ids.join(',');
						});
					}
				},
	            config: {
	                resolve: {
	                    scripts: ['lazyScript', function(lazyScript) {
	                        return lazyScript.register('assets/js/editor/ckeditor.js');
	                    }]
	                }
	            }
			}).then(function() {
				query(false);
			});
		}
		$scope.remove = function() {
			var ids = api.getGridSelectedIds($scope);
			dialog.confirm('确定要移除选中的设置？').result.then(function(r) {
				if (r) {
					http.post({
						name: 'notice_remove',
						params: {
							ids: ids
						},
						success: function() {
							query(false);
						}
					})
				}
			});
		}

		$scope.gridOptions = {
			paginationCurrentPage: api.page.pageNumber,
			paginationPageSize: api.page.pageSize,
			useExternalPagination: true,
			columns: [{
				name: '序号',
				width: 50,
				// field: 'id',
				cellTemplate: '<div class="x-grid-inner">{{(grid.options.paginationCurrentPage - 1) * grid.options.paginationPageSize + (index + 1)}}</div>'
			}, {
				name: '标题',
				width: 200,
				field: 'title',
				align: 'left'
			}, {
				name: '日期',
				width: 300,
				field: 'crtime',
				align: 'left' 
			}, {
				name: '详情',
				width: 100,
				field: 'id',
				cellTemplate: '<div class="x-grid-inner"><span class="btn-text" ng-click="grid.appScope.preview(grid.getCellValue(row, col))">查看</span></div>'
			}],
			forceFit: false,
			onRegisterApi: function(api) {
				$scope.api = api;
				api.pagination && api.pagination.on.paginationChanged($scope, function(newPage, pageSize) {
					query(false);
				});
			}
		};

		$scope.preview = function(id) {
			api.form({
				title: '公告详情',
				template: require('./views/form_preview.tpl.html'),
				scope: {
	                trustAsHtml: function(html) {
	                    return $sce.trustAsHtml(html);
	                }
				},
				resolveWait:{
						fields:function(){
							return http.post({
								name: 'notice_preview',
								params:{
									id: id
								}
							})
						}
				},
				resolveApplyScope: ['fields']
			})
		}

		$scope.hasSelected = function() {
			return api.hasGridSelected($scope);
		}
		$scope.hasSelectedRecords = function() {
			return api.hasGridSelectedRecords($scope);
		}

		query();
	}
}