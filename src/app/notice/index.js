import {alias} from 'config';
import controller from './Notice'

import template from './views/list.tpl.html';

alias({
    'notice_list': 'message/list',
    'notice_add': 'message/save',
    'notice_item': 'message/read',
    'notice_edit': 'message/updateMessage',
    'notice_remove': 'message/batchDeleteMessage',
    'notice_preview': 'message/detail'
})
const router = ($stateProvider) => {
    $stateProvider.state('notice', {
        url: '/notice',
        views: {
            content: {
                template,
                controller,
                controllerAs: 'vm'
            }
        },
        parent: 'app',
        data: {
            name: 'config',
            title: '通知公告'
        }
    });
}
router.$inject = ['$stateProvider'];

export default angular.module('app.notice', [])
    .config(router)
    .name;