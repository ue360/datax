import {alias} from 'config';
import Sms from './Sms';
import SmsNew from './SmsNew';
import SmsSend from './SmsSend';
import SmsRead from './SmsRead';

import template from 'templates/tab_layout.tpl.html';

alias({
    'sms_new': 'message/list',
    'sms_read': 'message/list',
    'sms_send': 'message/list',
    'sms_add': 'message/save',
    'sms_reply': 'message/save',
    'sms_remove': 'message/batchDeleteMessage',
    'sms_preview': 'message/read',
    'sms_msg_read': 'message/updateMsg'
})
const router = ($stateProvider) => {
    $stateProvider
        .state('sms', {
            abstract: true,
            views: {
                content: {
                    template,
                    controller: Sms,
                    controllerAs: 'vm'
                }
            },
            parent: 'app',
            data: {
                name: 'sms',
                title: '站内短信',
                tabs: [{
                    state: 'sms.new',
                    name: '未读消息'
                }, {
                    state: 'sms.read',
                    name: '已读消息'
                }, {
                    state: 'sms.send',
                    name: '已发消息'
                }]
            }
        })
        .state('sms.read', {
            url: '/sms/read',
            views: {
                "tabpanel": {
                    template: require('./views/list_read.tpl.html'),
                    controller: SmsRead,
                    controllerAs: 'vm'
                }
            },
            data: {
                title: '已读消息'
            }
        })
        .state('sms.send', {
            url: '/sms/send',
            views: {
                "tabpanel": {
                    template: require('./views/list_send.tpl.html'),
                    controller: SmsSend,
                    controllerAs: 'vm'
                }
            },
            data: {
                title: '已发消息'
            }
        })
        .state('sms.new', {
            url: '/sms/new',
            views: {
                "tabpanel": {
                    template: require('./views/list_new.tpl.html'),
                    controller: SmsNew,
                    controllerAs: 'vm'
                }
            },
            data: {
                title: '未读消息'
            }
        });
}
router.$inject = ['$stateProvider'];

export default angular.module('app.sms', [])
    .config(router)
    .name;