import {Inject} from 'plugins'
import config from 'config';

@Inject('$scope', '$state', 'ui.http', 'ui.dialog', 'ui.api')
export default class Sms {
	constructor($scope, $state, http, dialog, api) {
		var data = $state.current.data || {};
	    var tabs = $scope.tabs = data.tabs;

		$scope.activeIndex = 0;

		var changeState = $scope.changeState = function(state) {
			state = state || $state.current.name;
			for (var i=0,l=tabs.length; i<l; i++) {
				var tab = tabs[i];
				if (tab.state == state) {
					$scope.activeIndex = i;
					break;
				}
			}
		}
		// changeState();

		$scope.select = function(state) {
			if (angular.isNumber(state) && tabs[state]) {
				state = tabs[state].state;
			}
			state = state || $state.current.name;
			if (state) {
				if ($state.is(state)) {
					$state.reload(state);
				} else {
					$state.go(state);
				}
			}
		}

		$scope.change = function(index) {
			if ($scope.activeIndex == index) return;
			var tab = tabs[index];
			$scope.activeIndex = index;
			$scope.select(tab.state);
		}
	}
}