import {Inject} from 'plugins'
import config from 'config';

@Inject('$scope', '$state', 'ui.http', 'ui.dialog', 'ui.api', 'DepartService')
export default class SmsNew {
	constructor($scope, $state, http, dialog, api, DepartService) {
		var appScope = $scope.$parent;

	    appScope.changeState();

		var q = $scope.q = {};

		var loadData = $scope.loadData = function(params) {
			params = params ||{};
				angular.extend(params,{
					type : 0
				});
				api.gridLoad({
					name: 'sms_new', 
					params: params,
					scope: $scope
				});
			};

	    var query = $scope.query = function(resetPage) {
			if (resetPage === false || $scope.gridOptions.paginationCurrentPage == 1) {
				loadData(q);
			} else {
				api.gridReset(null, $scope);
			}
		}

		$scope.read = function() {
			var ids = api.getGridSelectedIds($scope);
			dialog.confirm('确定要设为已读？').result.then(function(r) {
				if (r) {
					http.post({
						name: 'sms_msg_read',
						params: {
							ids: ids , 
							type : 0
						},
						success: function() {
							query(false);
						}
					})
				}
			});
		}
		$scope.readAll = function() {
			dialog.confirm('确定要全部设为已读？').result.then(function(r) {
				if (r) {
					http.post({
						name: 'sms_msg_read',
						params: {
							type : 1
						},
						success: function() {
							query(false);
						}
					})
				}
			});
		}
		$scope.reply = function(grid, row) {
			var data = grid.getData(row);
			api.form({
				title: '回复', 
				template: require('./views/form.tpl.html'),
				name: 'sms_reply',
				data: {
					id: data.id
				},
				scope: {
					// 显示名字段，接入时注意修改成真实字段
					names: data.author,
					select: function(field) {
						var _scope = this;
						DepartService.select().then(function(users) {
							var ids = [];
							// 显示名
							_scope[field] = (users || []).map(function(r) {
								ids.push(r.id);
								return r.username;
							}).join(',');
							// 入库id集合
							_scope.data[field] = ids.join(',');
						});
					}
				},
	            config: {
	                resolve: {
	                    scripts: ['lazyScript', function(lazyScript) {
	                        return lazyScript.register('assets/js/editor/ckeditor.js');
	                    }]
	                }
	            }
			}).then(function() {
				query();
			});
		}
		$scope.remove = function() {
			var ids = api.getGridSelectedIds($scope);
			dialog.confirm('确定要移除选中的数据？').result.then(function(r) {
				if (r) {
					http.post({
						name: 'sms_remove',
						params: {
							ids: ids,
							type: 2
						},
						success: function() {
							query(false);
						}
					})
				}
			});
		}

		$scope.gridOptions = {
			paginationCurrentPage: api.page.pageNumber,
			paginationPageSize: api.page.pageSize,
			useExternalPagination: true,
			columns: [{
				name: '序号',
				width: 50,
				// field: 'id',
				cellTemplate: '<div class="x-grid-inner">{{(grid.options.paginationCurrentPage - 1) * grid.options.paginationPageSize + (index + 1)}}</div>'
			}, {
				name: '标题',
				width: 300,
				field: 'title',
				align: 'left'
			}, {
				name: '收件人',
				width: 300,
				field: 'userName'
			}, {
				name: '发送时间',
				width: 300,
				field: 'crtime',
				align: 'center'
			}, {
				name: '详情',
				width: 100,
				field: 'id',
				cellTemplate: '<div class="x-grid-inner"><span class="btn-text" ng-click="grid.appScope.preview(grid.getCellValue(row, col))">详情</span><span class="btn-text" ng-click="grid.appScope.reply(grid, row)">回复</span></div>'
			}],
			forceFit: false,
			onRegisterApi: function(api) {
				$scope.api = api;
				api.pagination && api.pagination.on.paginationChanged($scope, function(newPage, pageSize) {
					query(false);
				});
			}
		};

		$scope.preview = function(id) {
			api.form({
				title: '短消息详情',
				template: require('./views/form_preview.tpl.html'),
				resolveWait:{
					data:function(){
						return http.post({
							name: 'sms_preview',
							params: {
							id: id,
							type : 0
							}
						})
					}
				},
				resolveApplyScope:['data']
			})
		}

		$scope.hasSelected = function() {
			return api.hasGridSelected($scope);
		}
		$scope.hasSelectedRecords = function() {
			return api.hasGridSelectedRecords($scope);
		}

		$scope.$on('$viewContentLoaded', function() {
			query();
		})
	}
}