import {alias} from 'config';
import * as app from './components';
import template from './views/layout.tpl.html'

alias({
    'api_users': 'grid.json'
})
const router = ($stateProvider) => {
    $stateProvider.state('api', {
            abstract: true,
            views: {
                "content": {
                    template: require('templates/tab_layout.tpl.html'),
                    controller: app.Api
                }
            },
            data: {
                name: 'api',
                title: '开放接口',
                welcome: 'api.layout.info',
                tabs: [{
                    state: 'api.layout.type',
                    name: '接口分类列表'
                }, {
                    state: 'api.user',
                    name: '用户接口列表'
                }]
            },
            parent: 'app'
        })
        .state('api.layout', {
            abstract: true,
            views: {
                "tabpanel": {
                    controller: app.ApiLayout,
                    template
                }
            }
        })
        .state('api.layout.info', {
            url: '/api/info',
            views: {
                "primary": {
                    template: require('templates/welcome.tpl.html')
                }
            },
            data: {
                title: '欢迎页'
            }
        })
        .state('api.layout.type', {
            url: '/api/type',
            views: {
                "primary": {
                    template: require('./views/list_type.tpl.html'),
                    controller: app.Types
                }
            },
            data: {
                title: '接口分类列表'
            }
        })
        .state('api.user', {
            url: '/api/user',
            views: {
                "tabpanel": {
                    template: require('./views/list_user.tpl.html'),
                    controller: app.Users
                }
            },
            data: {
                title: '用户接口列表'
            }
        });
}
router.$inject = ['$stateProvider'];

export default angular.module('app.api', [])
    .config(router)
    .name;