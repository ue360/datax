import {Inject} from 'plugins'
import config from 'config';

@Inject('$scope', '$state', 'ui.http', 'ui.dialog', 'ui.api')
export class Users {
	constructor($scope, $state, http, dialog, api) {
		var appScope = $scope.$parent;

		appScope.changeState();

		var q = $scope.q = {};

		var loadData = $scope.loadData = function(params) {
				api.gridLoad({
					name: 'api_users', 
					params: params,
					scope: $scope,
	                success: function(data, options) {
	                    var _scope = this;
	                    _scope.fields = angular.merge({}, _scope.fields, data.fields);
	                }
				});
			};

	    var query = $scope.query = function(resetPage) {
			if (resetPage === false || $scope.gridOptions.paginationCurrentPage == 1) {
				loadData(q);
			} else {
				api.gridReset(null, $scope);
			}
		}

		$scope.add = function() {
			api.form({
				title: '新建用户', 
				template: require('../views/form_user.tpl.html'),
				name: 'api_user_add'
			}).then(function() {
				query();
			});
		}
		$scope.edit = function() {
			var id = api.getGridSelectedId($scope);

			api.form({
				title: '修改用户', 
				template: require('../views/form_user.tpl.html'),
				name: 'api_user_edit',
				resolveWait: http.post({
					name: 'api_user',
					params: {
						id: id
					}
				})
			}).then(function() {
				query(false);
			});
		}
		$scope.remove = function() {
			var ids = api.getGridSelectedIds($scope);
			dialog.confirm('确定要删除已选择的用户？').result.then(function(r) {
				if (r) {
					http.post({
						name: 'api_user_remove',
						params: {
							ids: ids
						},
						success: function() {
							query(false);
						}
					})
				}
			});
		}

		$scope.gridOptions = {
			paginationCurrentPage: api.page.pageNumber,
			paginationPageSize: api.page.pageSize,
			useExternalPagination: true,
			columns: [{
				name: '序号',
				width: 50,
				cellTemplate: '<div class="x-grid-inner">{{(grid.options.paginationCurrentPage - 1) * grid.options.paginationPageSize + (index + 1)}}</div>'
			}, {
				name: '用户名',
				align: 'left',
				width: 150,
				field: 'username'
			}, {
				name: '真实姓名',
				width: 150,
				field: 'realname'
			}, {
				name: '创建时间',
				width: 200,
				field: 'crtime'
			}, {
				name: '操作',
				width: 200,
				field: 'id',
				cellTemplate: '<div class="x-grid-inner"><span class="btn-text" ng-click="grid.appScope.preview(grid.getCellValue(row, col))">接口</span><span class="btn-text" ng-click="grid.appScope.logs(grid, row)">日志</span></div>'
			}],
			forceFit: false,
			onRegisterApi: function(api) {
				$scope.api = api;
				api.pagination && api.pagination.on.paginationChanged($scope, function(newPage, pageSize) {
					query(false);
				});
			}
		};
		
		$scope.mapping = function(grid, row, col, field) {
			return api.mapping({
				grid: grid,
				row: row,
				col: col,
	            field: field,
				scope: $scope
			});
		}

		// $scope.preview = function(id) {
		// 	api.form({
		// 		title: '接口详情',
		// 		template: require('./views/form_preview.tpl.html'),
		// 		resolveWait: http.post({
		// 			name: 'api_preview',
		// 			params: {
		// 				id: id
		// 			}
		// 		})
		// 	})
		// }

		$scope.logs = function(id) {
			var _loadData = function(params) {
				params = params || {};
				angular.extend(params, {
					id: id
				});
				return api.gridLoad({
					name: 'api_logs', 
					params: params,
					scope: this
				});
			};
			api.form({
				title: '日志记录',
				template: require('../views/form_logs.tpl.html'),
				scope: {
					q: {},
					gridOptions: {
						paginationCurrentPage: api.page.pageNumber,
						paginationPageSize: api.page.pageSize,
						useExternalPagination: true,
						multiSelect: false,
						columns: [{
							name: '序号',
							width: 50,
							cellTemplate: '<div class="x-grid-inner">{{index+1}}</div>'
						}, {
							name: '存档日期',
							field: 'date'
						}, {
							name: '操作',
							width: 150,
							field: 'id',
							cellTemplate: '<div class="x-grid-inner"><span class="btn-text" ng-click="grid.appScope.preview(grid.getCellValue(row, col))">查看详情</span></div>'
						}],
						data: [],
						onRegisterApi: function(api, grid) {
							var _scope = grid.appScope;
							_scope.api = api;

							api.pagination && api.pagination.on.paginationChanged(_scope, function(newPage, pageSize) {
								_scope.query(false);
							});
						}
					},
					query: function(resetPage, params) {
						var _scope = this;
						if (resetPage === false || _scope.gridOptions.paginationCurrentPage == 1) {
							var _params = angular.merge({}, _scope.q, params);
							_loadData.call(_scope, _params);
						} else {
							api.gridReset(null, _scope);
						}
					},
					preview: function(cid) {
						api.form({
							title: '日志详情',
							template: require('../views/form_preview.tpl.html'),
							resolveWait: http.post({
								name: 'api_log_preview',
								params: {
									id: id,
									cid: cid
								}
							})
						})
					}
				},
				resolveApply: false,
				resolveWait: function() {
					return _loadData.call(this);
				}
			})
		}

		$scope.hasSelected = function() {
			return api.hasGridSelected($scope);
		}
		$scope.hasSelectedRecords = function() {
			return api.hasGridSelectedRecords($scope);
		}

		$scope.$on('$viewContentLoaded', function() {
			query();
		})
	}
}