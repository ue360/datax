import {alias} from 'config'
import * as app from './components';

import * as service from './service';

alias({
    'meta_model_types': 'api/grid.json',
    'meta_model_type': 'category/get',
    'meta_models': 'cube/list',
    'meta_model': 'cube/get',
    'meta_model_options': 'cube/getOptions',
    'meta_fields': 'metric/list',
    'meta_fields_filter': 'metric/mlist',
    'meta_fields_query': 'api/fields.json',
    'meta_treeview_data': 'metric/searchMetricCube',
    'meta_field': 'api/field.json',
    'meta_field_add': 'metric/save',
    'meta_field_remove': 'metric/delete',
    'meta_field_edit': 'metric/edit',
    'meta_field_save': 'metric/edit',
    'meta_field_unique': 'cube/unique',
    'meta_field_styles': 'metric/get',
    'meta_field_style': 'style/getStyle',
    'meta_field_style_add': 'style/save',
    'meta_field_style_edit': 'style/edit',
    'meta_field_style_remove': 'style/delete',
    'meta_rules': 'colRuleRelation/list',
    'meta_rules_choose': 'colRuleRelation/gets',
    'meta_rule_add': 'colRuleRelation/save',
    'meta_rule_edit': 'colRuleRelation/save',
    'meta_rule_custom_add': 'colRuleRelation/save',
    'meta_rule': 'colRuleRelation/get',
    'meta_rule_remove': 'colRuleRelation/delete',
    'meta_model_rules_save': 'colRuleRelation/configure',
    'meta_model_rule_remove': 'colRuleRelation/remove',
    'meta_model_fields': 'metric/cubeMetricsList',
    'meta_model_rules': 'colRuleRelation/cubeInspectList',
    'meta_model_fields_save': 'cube/configure',
    'meta_model_field_remove': 'cube/remove',
    'meta_model_field_move_prev': 'cube/moveUp',
    'meta_model_field_move_next': 'cube/moveDown',
    'meta_model_order_desc': 'api/field.json',
    'meta_model_type_add': 'category/save',
    'meta_model_type_edit': 'category/edit',
    'meta_model_type_remove': 'category/delete',
    'meta_model_add': 'cube/save',
    'meta_model_edit': 'cube/edit',
    'meta_model_remove': 'cube/delete',
    'meta_rules_add': 'colRuleRelation/metricRuleconfigure',
    'meta_rules_filter': 'colRuleRelation/metricRulesList',
    'meta_options': 'api/grid.json',
    'meta_options_filter': 'style/get',
    'check_cube': 'cube/checkCube',
    'org_models': 'api/grid.json',
    'org_model': 'api/model.json'
})

const router = ($stateProvider) => {
    $stateProvider
        .state('meta', {
            abstract: true,
            data: {
                title: '指标管理'
            },
            parent: 'app'
        })
        .state('meta.model', {
            abstract: true,
            views: {
                "content": {
                    template: require('./views/list_model.tpl.html'),
                    controller: app.MetaModel
                }
            },
            data: {
                name: 'credit',
                title: '资源目录'
            },
            parent: 'app'
        })
        .state('meta.model.info', {
            url: '/meta/model/info',
            views: {
                "primary": {
                    template: require('templates/welcome.tpl.html')
                }
            }
        })
        .state('meta.model.types', {
            url: '/meta/model/types',
            views: {
                "primary": {
                    template: require('./views/include_model_types.tpl.html'),
                    controller: app.MetaModelTypes
                }
            }
        })
        .state('meta.model.list', {
            url: '/meta/models',
            views: {
                "primary": {
                    template: require('./views/include_models.tpl.html'),
                    controller: app.MetaModelList
                }
            }
        })
        .state('meta.fields', {
            url: '/meta/fields',
            views: {
                "content": {
                    template: require('./views/list_field.tpl.html'),
                    controller: app.MetaFieldList
                }
            },
            parent: 'app',
            data: {
                name: 'credit',
                title: '指标管理'
            }
        })
        .state('meta.treeview', {
            url: '/meta/treeview?id',
            views: {
                "content": {
                    template: require('./views/list_treeview.tpl.html'),
                    controller: app.MetaTreeview
                }
            },
            parent: 'app',
            data: {
                name: 'credit',
                title: '指标立方'
            },
            resolve: {
                scripts: ['lazyScript', function(lazyScript) {
                    return lazyScript.register([
                        'assets/js/vis.min.js',
                        'assets/js/d3.min.js'
                    ]);
                }],
                $Field: ['$q', '$stateParams', 'ui.http', function($q, $stateParams, http) {
                    var defered = $q.defer();
                    var id = $stateParams.id;
                    if (id == null) {
                        return;
                    }
                    http.post({
                        name: 'meta_field',
                        params: {
                            id: id
                        },
                        success: function(data) {
                            defered.resolve(data);
                        }
                    })
                    return defered.promise;
                }]
            }
        })
        .state('meta.rules', {
            url: '/meta/rules',
            views: {
                "content": {
                    template: require('./views/list_rule.tpl.html'),
                    controller: app.MetaRuleList
                }
            },
            parent: 'app',
            data: {
                name: 'credit',
                title: '字段规则'
            }
        });
}
router.$inject = ['$stateProvider'];

export default angular.module('app.meta', [])
    .config(router)
    .factory('MetaFieldService', service.field)
    .factory('MetaModelService', service.model)
    .name;