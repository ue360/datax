export const model = (http, dialog, api) => {
	var service = {
		add: function(appScope) {
			appScope = appScope || this;
			var node = appScope.getTreeNode();
			var id = node.id;
			api.form({
				title: '新建分类', 
				template: require('../views/form_model_type.tpl.html'), 
				name: 'meta_model_type_add',
				data: {
					parentid: id
				}
			}).then(function() {
				appScope.treeReload();
			});
		},
		edit: function(appScope) {
			appScope = appScope || this;
			var node = appScope.getTreeNode();
			var id = node.id;

			api.form({
				title: '编辑分类', 
				template: require('../views/form_model_type.tpl.html'), 
				name: 'meta_model_type_edit',
				resolveWait: http.post({
						name: 'meta_model_type',
						params: {
							id: id
						}
					})
			}).then(function() {
				appScope.treeReload();
			});
		},
		remove: function(appScope) {
			appScope = appScope || this;
			var node = appScope.getTreeNode();
			var id = node.id;

			dialog.confirm('确定要删除当前分类？').result.then(function(r) {
				if (r) {
					http.post({
						name: 'meta_model_type_remove',
						params: {
							id: id
						},
						success: function() {
							appScope.treeReload();
						}
					})
				}
			});
		}
	}
	return service;
}
model.$inject = ['ui.http', 'ui.dialog', 'ui.api'];