import {
	Inject
} from 'plugins'
import config from 'config';

@Inject('$scope', '$state', 'ui.dialog', 'ui.http', 'ui.api', 'MetaModelService')
export class MetaModelTypes {
	constructor($scope, $state, dialog, http, api, service) {
		var appScope = $scope.$parent;

		function getTreeNode() {
			return appScope.getTreeNode();
		}
		var _node = getTreeNode();
		if (!_node) {
			appScope.select('meta.model.info');
			return;
		} else {
			var tree = appScope.tree;
			var root = tree.dataSource.at(0);
			if (!appScope.isNode()) {
				api.treeSelect(appScope.tree, root.id);
				return;
			}
		}

		$scope.treeReload = function() {
			return api.treeListReload(true, $scope);
		}

		var dataSource = new kendo.data.TreeListDataSource({
			transport: {
				read: {
					url: config.apiRootUrl + "category/treeCategory",
					dataType: 'json',
					data: {
						id: _node.id
					}
				}
			},
			schema: api.schema
		});

		$scope.treeListOptions = {
			dataSource: dataSource,
			sortable: true,
			editable: false,
			resizable: true,
			columns: [{
				field: "text",
				expandable: true,
				title: "目录名",
				template: '<span class="k-node"><i class="fa fa-folder-open"></i> {{dataItem.text}}</span>'
			}, {
				field: "author",
				title: "创建人"
			}, {
				field: "data",
				title: "创建时间"
			}]
		};
	}
}