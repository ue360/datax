import {alias} from 'config';
import controller from './Dictionary'

import template from './views/list.tpl.html';

alias({
    'dictionary': 'dictionary/list',
    'dictionary_add': 'dictionary/save',
    'dictionary_edit': 'dictionary/updateDictionary',
    'dictionary_remove': 'dictionary/delete',
    'dictionary_start': 'dictionary/batchEnableDictionary',
    'dictionary_stop': 'dictionary/batchDisableDictionary',
    'dictionary_preview': 'dictionary/get',
    'dictionary_checkIsExist': 'dictionary/checkDictionaryName',
    'dictionary_item': 'dictionary/get'
})
const router = ($stateProvider) => {
    $stateProvider.state('dictionary', {
        url: '/dictionary',
        views: {
            content: {
                template,
                controller,
                controllerAs: 'vm'
            }
        },
        parent: 'app',
        data: {
            name: 'credit',
            title: '字典管理'
        }
    });
}
router.$inject = ['$stateProvider'];

export default angular.module('app.dictionary', [])
    .config(router)
    .name;