import AppConfig, {alias} from './constant'
import AppSystem from './system'
import interceptor from './interceptor'

export {
	AppConfig,
	alias,
	AppSystem,
	interceptor
}
export default AppConfig