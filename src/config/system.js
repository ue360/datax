// 公共模版
const templates = [
    'layout.tpl.html',
    'upload.tpl.html',
    'upload_type.tpl.html',
    'tab_layout.tpl.html',
    'tree.tpl.html',
    'welcome.tpl.html'
]

const AppSystem = ($rootScope, $templateCache, $state, $stateParams, dialog, http, api, APP_CONFIG, User) => {
    for (let template of templates) {
        $templateCache.put(APP_CONFIG.rootPath + "app/misc/views/" + template, require('templates/' + template));
    }

	$rootScope.$state = $state;
    $rootScope.$stateParams = $stateParams;
    $rootScope.rootPath = APP_CONFIG.rootPath;
    $rootScope.global = {};
    $rootScope.logout = () => {
        // $rootScope.$emit("userIntercepted");
        dialog.confirm('确定要退出吗？').result.then((r) => {
            if (r) {
                http.post({
                    name: 'logout',
                    success: () => {
                        $state.go('login');
                    }
                })
            }
        });
    }
    $rootScope.modify = function() {
        var user = User.get();
        if (!user) {
            dialog.alert('发生异常！');
            return;
        }
        api.form({
            title: '修改个人信息',
            template: require('../app/org/user/views/form_modify.tpl.html'),
            name: 'org_user_modify',
            resolveWait: http.post({
                name: 'org_user',
                params: {
                    id: user.id
                }
            })
        })
    }
    $rootScope.download = (url) => {
        var iframe = document.getElementById('__download');
        if (!iframe) {
            iframe = document.createElement('iframe');
            iframe.style.display = 'none';
            iframe.setAttribute('id', '__download');
            document.body.appendChild(iframe);
        }

        iframe.src = APP_CONFIG.apiRootUrl + url + (url.indexOf('?') === -1 ? '?' : '&') + 'temp='+ (new Date().getTime());
    }

    $rootScope.$on('userIntercepted', (errorType) => {
        $state.go('login');
    });
    $rootScope.$on('error404', (errorType) => {
        // $state.go('404');
    });
    $rootScope.$on('error500', (errorType) => {
        // $state.go('500');
    });
    $rootScope.$on('accessDenied', (errorType) => {
        $state.go('forbidden');
    });

    // 拦截器
    $.ajaxSetup({
        beforeSend: (xhr, settings) => {
            // xhr.setRequestHeader('Authorization', 'Token 123')
        }
    });
    $(document)
        .ajaxSuccess((event, response, settings) => {
            // console.log(response.status);
        })
        .ajaxError((event, request, settings) => {
            if (request.status === 400) {
                var text = request.responseText;
                try {
                    var r = JSON.parse(text);
                    dialog.alert(r.error);
                } catch(e) {}
                
            } else if (request.status === 403) {
                $rootScope.$emit("userIntercepted", "notLogin", request);
            } else if (request.status === 404) {
                $rootScope.$emit("error404", "404", request);
            } else if (request.status === 500) {
                $rootScope.$emit("error500", "500", request);
            } else if (request.status === 401) {
                $rootScope.$emit("accessDenied", "forbidden", request);
            }
        });
}

AppSystem.$inject = ['$rootScope', '$templateCache', '$state', '$stateParams', 'ui.dialog', 'ui.http', 'ui.api', 'APP_CONFIG', 'User']

export default AppSystem