const AppConfig = {
	rootPath: '',
	apiRootUrl: '',
	appid: 'portal_ui_intertid',
	apiPaths: {},
	upload: (type) => {
        let config = {
                
            };
        switch(type) {
            case 'image':
                return {
                    ...config,
                    postList: 'pic_gallery',
                    postUpload: 'pic/uploadPic.jhtml',
                    postGroupAdd: 'pic_group_add',
                    postGroupEdit: 'pic_group_edit',
                    postGroupRemove: 'pic_group_remove',
                    postItemRemove: 'pic_item_remove',
                    accept: {
                        extensions: 'bmp,png,jpeg,jpg,gif',
                        mimeTypes: 'image/bmp,image/png,image/jpeg,image/jpg,image/gif'
                    }
                }
                // angular.merge(config, {
                //     postList: 'gallery',
                //     postUpload: 'upload.json',
                //     accept: {
                //         extensions: 'bmp,png,jpeg,jpg,gif',
                //         mimeTypes: 'image/bmp,image/png,image/jpeg,image/jpg,image/gif'
                //     }
                // })
                break;
        }
        return config;
    }
}

const alias = (states) => {
	const {apiPaths} = AppConfig;

	AppConfig.apiPaths = {
		...apiPaths,
		...states
	}
}

export default AppConfig;

export {
	alias
}