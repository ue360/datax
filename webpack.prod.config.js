// const ExtractTextPlugin = require("extract-text-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const CleanWebpackPlugin = require('clean-webpack-plugin');

const path = require('path');
const merge = require('webpack-merge') // 专用合并 webpack 配置的包
const webpackConfig = require('./webpack.config');
const distPath = path.resolve(__dirname, './build');

module.exports = merge(webpackConfig, {
	output: {
		filename: 'js/[name].js',
		path: distPath
	},
	// 生产模式配置
	module: {
		// rules: [{
		// 	test: /\.less$/,
		// 	use: ExtractTextPlugin.extract({
		// 		fallback: "style-loader",
		// 		use: [{
		// 			loader: "css-loader",
		// 			options: {
		// 				minimize: true
		// 			}
		// 		}, {
		// 			loader: "less-loader"
		// 		}]
		// 	})
		// }]
		rules: [{
			test: /\.less$/,
			use: [{
				loader: MiniCssExtractPlugin.loader,
				options: {
					// 复写 css 文件中资源路径
					// webpack3.x 配置在 extract-text-webpack-plugin 插件中
					// 因为 css 文件中的外链是相对与 css 的，
					// 我们抽离的 css 文件在可能会单独放在 css 文件夹内
					// 引用其他如 img/a.png 会寻址错误
					// 这种情况下所以单独需要配置 publicPath，复写其中资源的路径
					publicPath: '../'
				}
			}, {
				loader: "css-loader",
				options: {
					minimize: true
				}
			}, {
				loader: "less-loader"
			}]
		}]
	},
	plugins: [
		new CleanWebpackPlugin([distPath]),
		// new ExtractTextPlugin({
		// 	filename: "assets/css/app.css",
		// 	allChunks: true
		// })
		new MiniCssExtractPlugin({
			// 输出到单独的 css 文件夹下
			filename: "assets/css/[name].plugin.css"
		})
	]
})