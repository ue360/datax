const path = require('path');
const webpack = require('webpack');

const HtmlWebpackPlugin = require('html-webpack-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');

const srcPath = path.resolve(__dirname, './src');
const distPath = path.resolve(__dirname, './build');

module.exports = {
	entry: {
		app: './src/app/index.js'
	},
	output: {
		filename: 'js/[name].[hash].js',
		path: distPath
	},
	module: {
		rules: [{
			test: /\.(js)$/,
			loader: 'babel-loader',
			exclude: /node_modules/
		}, {
			test: /\.(js)$/,
			loader: 'eslint-loader',
			enforce: 'pre',
			exclude: /node_modules/
		}, {
			test: /\.tpl\.html$/,
			loader: 'html-loader',
			query: {
				interpolate: true,
				minimize: true
			},
			exclude: /(node_modules)/,
			include: path.join(__dirname, 'src/app')
		}/*, {
			test: /\.tpl\.html$/,
			loader: 'file-loader?name=[path][name]-[hash:8].[ext]',
			exclude: /(node_modules)/,
			include: path.join(__dirname, 'src/app')
		}*/, {
			test: /\.css$/,
			loader: "style-loader!css-loader!postcss-loader"
		}, {
			test: /\.less$/,
			loader: "style-loader!css-loader!postcss-loader!less-loader"
		}, {
			test: /\.json$/,
			loader: "json-loader"
		}, {
			test: /\.(jpe?g|png|gif)$/,
			loader: 'url-loader',
			query: {
				name: '[name].[ext]',
				publicPath: "../images/",
          		outputPath: "./assets/images/"
			}
		}, {
			/**
			 * 末尾 \?.* 匹配带 ? 资源路径
			 * 我们引入的第三方 css 字体样式对字体的引用路径中可能带查询字符串的版本信息
			 */
			test: /\.(woff2?|svg|eot|ttf|otf)(\?.*)?$/,
			/**
			 * url-loader
			 * 会配合 webpack 对资源引入路径进行复写，如将 css 提取成独立文件，可能出现 404 错误可查看 提取 js 中的 css 部分解决
			 * 会以 webpack 的输出路径为基本路径，以 name 配置进行具体输出
			 * limit 单位为 byte，小于这个大小的文件会编译为 base64 写进 js 或 html
			 */
			loader: 'url-loader',
			query: {
				limit: 10000,
				name: '[name].[ext]',
				publicPath: "../fonts/",
          		outputPath: "./assets/fonts/"
			}
		}]
	},
	resolve: {
		extensions: ['.js'],
		modules: ['node_modules', './src/app'],
		alias: {
			config: path.resolve(__dirname, './src/config'),
			plugins: path.resolve(__dirname, './src/plugins'),
			assets: path.resolve(__dirname, './src/assets'),
			styles: path.resolve(__dirname, './src/components/styles'),
			templates: path.resolve(__dirname, './src/app/misc/views')
		}
	},
	performance: {
		hints: false
	},
	// optimization: {
	// 	splitChunks: {
	// 		cacheGroups: {
	// 			vendor: {
	// 				name: "vendor",
	// 				chunks: "initial",
	// 				minChunks: 2
	// 			}
	// 		}
	// 	}
	// },
	plugins: [
		new CopyWebpackPlugin([{
				from: './src/index.html'
			}, {
				from: './src/assets',
				to: 'assets'
			}]),
		new HtmlWebpackPlugin({
			filename: 'index.html', // 文件写入路径，前面的路径与 devServer 中 contentBase 对应
			template: './src/index.html', // 模板文件路径
			inject: true
		})
	],
	externals: {
		"angular": "angular",
		"$": "jQuery"
	}
}